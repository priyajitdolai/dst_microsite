import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { pages } from '@app/pages';
import { services } from '@app/services';
import { pipes } from '@app/pipes';
import { guards } from '@app/shared/guards';
import { components } from '@app/components';
import { resolvers } from '@app/shared/resolvers';
import { RECAPTCHA_LANGUAGE, RECAPTCHA_V3_SITE_KEY, RecaptchaModule, ReCaptchaV3Service } from 'ng-recaptcha';
import { environment } from '@environments/environment';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        ...pages,
        ...pipes,
        ...components,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            defaultLanguage: 'en',
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient],
            },
        }),
        ReactiveFormsModule,
        RecaptchaModule,
        NgSelectModule
    ],
    providers: [
        ...guards,
        ...services,
        ...resolvers,
        {
            provide: RECAPTCHA_LANGUAGE,
            useValue: environment.defaultLang,
        },
        {
            provide: RECAPTCHA_V3_SITE_KEY,
            useValue: environment.recaptchaKey,
        },
        ReCaptchaV3Service,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}

import { Type } from '@angular/core';
import { SpinnerComponent } from '@components/spinner/spinner.component';
import { CheckCodeComponent } from '@components/check-code/check-code.component';
import { ClearComponent } from '@components/clear.component';
import { VoucherDetailsComponent } from '@components/voucher-details/voucher-details.component';
import { OfferDetailsComponent } from '@components/offer-details/offer-details.component';
import { ResultsComponent } from '@components/results/results.component';
import { RedeemComponent } from '@components/redeem/redeem.component';

export const components: Type<any>[] = [
    SpinnerComponent,
    CheckCodeComponent,
    ClearComponent,
    OfferDetailsComponent,
    VoucherDetailsComponent,
    ResultsComponent,
    RedeemComponent,
];

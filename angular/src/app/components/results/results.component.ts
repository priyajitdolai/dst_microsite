import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SubscriptionComponent } from '@components/subscription.component';
import { FormGroup } from '@angular/forms';
import { VoucherDetailInterface } from '@app/shared/interfaces/voucher-detail.interface';

@Component({
    selector: 'app-results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.scss'],
})
export class ResultsComponent extends SubscriptionComponent implements OnInit{

    @Input() voucher: VoucherDetailInterface;
    @Input() resetData: boolean;
    @Output() voucherChange: EventEmitter<VoucherDetailInterface> = new EventEmitter<VoucherDetailInterface>();
    @Output() redeemChange: EventEmitter<boolean> = new EventEmitter<boolean>();
    form: FormGroup;

    constructor(
    ) {
        super();
    }
    ngOnInit() {
    }

    updateVoucher(voucher: VoucherDetailInterface) {
        this.voucherChange.emit(voucher);
    }
}

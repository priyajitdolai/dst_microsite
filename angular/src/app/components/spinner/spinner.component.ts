import { Component, Input, OnInit } from '@angular/core';
import { LoaderService } from '@services/loader.service';
import { SubscriptionComponent } from '@components/subscription.component';

@Component({
    selector: 'app-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.scss'],
})
export class SpinnerComponent extends SubscriptionComponent implements OnInit {
    @Input()
    color = '#3f3f3f';
    value = 50;
    show = false;

    constructor(
        private loaderService: LoaderService,
    ) {
        super();
    }

    ngOnInit() {
        this.subscriptions = this.loaderService.onLoadingChange().subscribe((show: boolean) => {
            setTimeout(() => {
                this.show = show;
            }, 0);
        });
    }

    get borderColor() {
        return {
            'border-top-color': this.color,
            'border-left-color': this.color,
            'border-bottom-color': this.color,
        };
    }
}

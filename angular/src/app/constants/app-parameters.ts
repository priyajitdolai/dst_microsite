export const AppParameter = {
    userLoginAttempts: 5,
    adminLoginAttempts: 10,
    loginBlockDuration: 5, // minutes
};

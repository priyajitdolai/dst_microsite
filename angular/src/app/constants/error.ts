export enum ErrorConst {
    general = 'An error occurred: ',
    ssoTokenError = 'SSO token error',
}

import { CheckHelper } from '@helpers/check.helper';

export class ArrayHelper {
    public static findById(array, object): boolean {
        let flag = false;
        if (array) {
            array.forEach(item => {
                if (item.id === object.id) {
                    flag = true;
                }
            });
        }

        return flag;
    }

    public static removeElement(array, element) {
        if (typeof element === 'object' && element.hasOwnProperty('id')) {
            return ArrayHelper.removeElementById(array, element);
        }

        return array.filter(item => item !== element);
    }

    public static removeElementById(array, element) {
        if (!element.hasOwnProperty('id')) {
            return array;
        }

        return array.filter(item => item.id !== element.id);
    }

    public static uniquePush(array, element) {
        if (!array.includes(element)) {
            array.push(element);
        }
    }

    public static uniqueObjectPush(array, element, ...properties: string[]) {
        if (!properties.length) {
            properties = ['id'];
        }

        let elementFlag = true;
        let arrayFlag = true;

        properties.forEach(property => {
            if (!element.hasOwnProperty(property)) {
                elementFlag = false;
            }

            if (array.length) {
                Object.values(array).forEach(item => {
                    if (!Object.keys(item).includes(property)) {
                        arrayFlag = false;
                    }
                });
            }
        });

        let canPush = false;
        if (elementFlag && arrayFlag) {
            properties.forEach(property => {
                const values = array.map(item => item[property]);
                if (!values.includes(element[property])) {
                    canPush = true;
                }
            });
        }

        if (canPush) {
            array.push(element);
        }
    }

    public static removeObjectDuplicates(array, ...properties: string[]) {
        const arr = [];
        array.forEach(item => {
            ArrayHelper.uniqueObjectPush(arr, item, ...properties);
        });

        return arr;
    }

    public static prepareIds(array) {
        const ids = [];
        for (const item of array) {
            if (!item.hasOwnProperty('id') && !CheckHelper.isNumber(item)) {
                continue;
            } else if (item.hasOwnProperty('id')) {
                ids.push(item.id);
                continue;
            }

            ids.push(item);
        }

        return ids;
    }

    public static findIndex(array, value) {
        for (const item of Object.entries(array)) {
            if (item[1] === value) {
                return item[0];
            }
        }

        return null;
    }
}

export class CheckHelper {
    public static isDefined(property) {
        return property !== null && property !== undefined;
    }

    public static isNumber(value) {
        return typeof value === 'number' || (value && !isNaN(Number(value)));
    }

    public static isFilled(value) {
        if (typeof value === 'number') {
            return true;
        }

        if (typeof value === 'string') {
            return value.trim() !== '';
        }

        if (Array.isArray(value)) {
            return value.length > 0;
        }

        if (typeof value === 'object') {
            return CheckHelper.objectFieldsFilled(value);
        }

        return false;
    }

    public static objectFieldsFilled(object: object, ...fields) {
        let flag = true;

        if (fields.length) {
            Object.entries(object).forEach(([key, value]) => {
                fields.forEach(item => {
                    if (item === key) {
                        if (!CheckHelper.isDefined(value) || !CheckHelper.isFilled(value)) {
                            flag = false;
                        }
                    }
                });
            });
        } else {
            Object.entries(object).forEach(([key, value]) => {
                if (!CheckHelper.isDefined(value) || !CheckHelper.isFilled(value)) {
                    flag = false;
                }
            });
        }

        return flag;
    }
}

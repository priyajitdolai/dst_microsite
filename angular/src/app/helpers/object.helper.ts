export class ObjectHelper {
    public static isEmpty(object): boolean {
        if (typeof object !== 'object') {
            return Boolean(object || false);
        }

        let flag = true;
        Object.values(object).forEach(value => {
            if (typeof value === 'object' && !ObjectHelper.isEmpty(value)) {
                flag = false;
            }

            if (value) {
                flag = false;
            }
        });

        return flag;
    }
}

import { Type } from '@angular/core';
import { LoginPageComponent } from '@app/pages/login/login-page.component';
import { RootPageComponent } from '@pages/root/root-page.component';
import { LoginBlockedPageComponent } from '@pages/login/login-blocked/login-blocked-page.component';

export const pages: Type<any>[] = [
    LoginPageComponent,
    LoginBlockedPageComponent,
    RootPageComponent,
];

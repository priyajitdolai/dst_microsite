import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'nl2br'})
export class Nl2brPipe implements PipeTransform {
    transform(value: string): string {
        if (typeof value !== 'string') {
            return value;
        }

        if (!value) {
            return '';
        }

        return value.replace(/(?:\r\n|\r|\n|\\n)/g, '<br />');
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { AuthStorage } from '@app/utils/auth-storage';

@Injectable()
export class HttpService {
    static readonly DEFAULT_HEADERS = {
        Authorization: null,
        Accept: 'application/json'
    };

    constructor(
        private http: HttpClient
    ) {
    }

    public static getDefaultHeaders(headers = {}) {
        const newHeaders = {} as any;

        Object.entries(this.DEFAULT_HEADERS).forEach(([key, value]) => {
            if (key === 'Authorization') {
                const authData = AuthStorage.getAuthData();
                if (authData && authData.token) {
                    newHeaders[key] = `Bearer ${authData.token}`;
                }
            } else {
                newHeaders[key] = value;
            }
        });

        if (typeof headers === 'object') {
            Object.entries(headers).forEach(([key, value]) => {
                if (key !== 'Authorization') {
                    newHeaders[key] = value;
                }
            });
        }

        return newHeaders;
    }

    public get<T>(api, options: any = {}, useExternalApi: boolean = false): Observable<T> {
        options = {
            ...options,
            headers: HttpService.getDefaultHeaders(options.headers),
        };

        return this.http.get<T>(useExternalApi ? environment.externalApiUrl + '/' + api : environment.baseUrl + '/api/' + api, options).pipe(
            map((response) => ({...response as any} as T)),
            catchError((err) => {
                return throwError(err);
            }),
        );
    }

    public getAsPromise<T>(api, options: any = {}) {
        return this.get<T>(api, options).toPromise();
    }

    public put<T>(api, body, options: any = {}) {
        options = {
            ...options,
            headers: HttpService.getDefaultHeaders(options.headers),
        };

        return this.http.put<T>(environment.baseUrl + '/api/' + api, body, options).pipe(
            map((response) => ({...response as any} as T)),
            catchError((err) => {
                return throwError(err);
            })
        );
    }

    public putAsPromise<T>(api, body, options: any = {}) {
        return this.put<T>(api, body, options).toPromise();
    }

    public post<T>(api, body, options: any = {}, useExternalApi: boolean = false) {

        if (useExternalApi && body) {
            body['apiVersion'] = environment.apiVersion;
        }

        options = {
            ...options,
            headers: HttpService.getDefaultHeaders(options.headers),
        };

        const url = useExternalApi ? environment.externalApiUrl + '/' + api : environment.baseUrl + '/api/' + api;
        return this.http.post<T>(url, body, options).pipe(
            map((response) => ({...response as any} as T)),
            catchError((err) => {
                return throwError(err);
            })
        );
    }

    public postAsPromise<T>(api, body, options: any = {}, useExternalApi: boolean = false) {
        return this.post<T>(api, body, options, useExternalApi).toPromise();
    }

    public delete(api, options: any = {}) {
        options = {
            ...options,
            headers: HttpService.getDefaultHeaders(options.headers),
        };

        return this.http.delete(environment.baseUrl + '/api/' + api, options).pipe(
            map((response) => ({...response as any})),
            catchError((err) => {
                return throwError(err);
            })
        );
    }

    public deleteAsPromise(api, options: any = {}) {
        return this.delete(api, options).toPromise();
    }

}

import { Type } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptor } from '@services/http.interceptor';
import { SecurityService } from '@services/security.service';
import { HttpService } from '@services/http.service';
import { LoaderService } from '@services/loader.service';
import { VoucherService } from '@services/voucher.service';

export const services: Type<any>[] = [
    SecurityService,
    HttpService,
    LoaderService,
    VoucherService,
    {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptor, multi: true} as any,
];

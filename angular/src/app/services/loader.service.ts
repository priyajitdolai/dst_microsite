import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, Subject, throwError } from 'rxjs';
import { catchError, map, shareReplay } from 'rxjs/operators';

@Injectable()
export class LoaderService {
    private loadingStream: Subject<boolean> = new ReplaySubject();
    constructor(
    ) {
    }

    public show() {
        this.loadingStream.next(true);
    }

    public hide() {
        this.loadingStream.next(false);
    }

    public onLoadingChange() {
        return this.loadingStream.pipe(
            map((data: boolean) => data),
            shareReplay()
        );
    }

    public loadingPipe(observable: Observable<any>) {
        return observable.pipe(
            map(response => {
                this.hide();
                return response;
            }),
            catchError(err => {
                this.hide();
                return throwError(err);
            })
        );
    }
}

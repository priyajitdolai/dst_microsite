import { Injectable, OnDestroy } from '@angular/core';
import { LogAction } from '@app/shared/enum/log-action.enum';
import { ExternalUserDetailInterface } from '@app/shared/interfaces/external-user-detail.interface';
import { LogActionInterface } from '@app/shared/interfaces/log-action.interface';
import { HttpService } from '@services/http.service';
import { AuthStorage } from '@app/utils/auth-storage';
import { AuthDataInterface } from '@app/shared/interfaces/auth-data.interface';
import { LoaderService } from '@services/loader.service';
import { Observable, ReplaySubject, Subject, Subscription } from 'rxjs';
import { UserDetailInterface } from '@app/shared/interfaces/user-detail.interface';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthDataExternalInterface } from '@app/shared/interfaces/auth-data-external.interface';
import { HttpResponseStatus } from '@app/shared/enum/http-response-status.enum';
import { environment } from '@environments/environment';
import { Endpoint } from '@app/shared/enum/endpoint.enum';
import { ErrorConst } from '@constants/error';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';

@Injectable()
export class SecurityService implements OnDestroy {
    private meObserver: Subject<any> = new ReplaySubject();
    private subscription: Subscription;
    // tslint:disable-next-line:variable-name
    private _me: Observable<UserDetailInterface> = this.pipeMyObservable(this.meObserver);

    constructor(
        private httpService: HttpService,
        protected loaderService: LoaderService,
        protected router: Router,
    ) {
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.subscription = null;
        }
    }

    login({username, password, remember_me}) {
        this.loaderService.show();
        return this.httpService.postAsPromise<AuthDataInterface>('login_check', {
            _username: username,
            _password: password,
        }).then(response => {
            AuthStorage.setAuthData(response, remember_me || false);
            this.getMeAsObservable().toPromise().then((user) => {

                const logAction: LogActionInterface = {
                    username,
                    action: LogAction.ACTION_LOGIN,
                    status: LogAction.STATUS_SUCCESS,
                    values: {
                        username,
                        password
                    }
                };
                this.httpService.post(Endpoint.actionLog, logAction).subscribe();

                AuthStorage.setCurrentUser(user);
                this.router.navigate(['/', 'dashboard']).then(() => {
                    this.loaderService.hide();
                });
            });
        });
    }

    async loginInExternalApi({username, password, remember_me}) {
        this.loaderService.show();

        const requestTokenBody = new HttpParams({
            fromObject: {
                grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                assertion: environment.ssoJwtToken
            }
        });
        await this.httpService.post(`api/v1/oauth/token`, requestTokenBody, {}, true).toPromise().then((response: any) => {
            if (!response.access_token) {
                throw ErrorConst.ssoTokenError;
            }
            AuthStorage.setAuthData({ token: response.access_token }, remember_me);
        }).catch(() => {
            throw ErrorConst.ssoTokenError;
        });

        return this.httpService.postAsPromise<AuthDataExternalInterface>(
            `api/v1/cms/getresellerdetails`,
            {
                requestId: `evolving_${moment(new Date()).format('DDMMYYYY')}`,
                data: {
                    apiVersion: environment.apiVersion,
                    loginName: username,
                    password,
                }
            }, {}, true).then((data: any) => {
            if (data.result?.responseCode === HttpResponseStatus.SUCCESS) {

                if (environment.logUserActions) {
                    const logAction: LogActionInterface = {
                        username,
                        action: LogAction.ACTION_LOGIN,
                        status: LogAction.STATUS_SUCCESS,
                        values: {
                            username,
                            password
                        }
                    };
                    this.httpService.post(Endpoint.actionLog, logAction).subscribe();
                }

                const user: ExternalUserDetailInterface = {
                    id: null,
                    username: data.result.resellerDetails.resellerDisplay,
                    email: data.result.resellerDetails.resellerEmail,
                    password,
                    enabled: data.result.resellerDetails.resellerActive === 'active'
                };
                AuthStorage.setCurrentUser(user);
                this.router.navigate(['/', 'dashboard']).then(() => {
                    this.loaderService.hide();
                });
            } else {
                if (environment.logUserActions) {
                    const logAction: LogActionInterface = {
                        username,
                        action: LogAction.ACTION_LOGIN,
                        status: LogAction.STATUS_ERROR,
                        values: {
                            username,
                            password
                        }
                    };
                    this.httpService.post(Endpoint.actionLog, logAction).subscribe();
                }

                throw data;
            }

        }, (error) => {

            if (environment.logUserActions) {
                const logAction: LogActionInterface = {
                    username,
                    action: LogAction.ACTION_LOGIN,
                    status: LogAction.STATUS_ERROR,
                    values: {
                        username,
                        password
                    }
                };
                this.httpService.post(Endpoint.actionLog, logAction).subscribe();
            }

            throw error;
        });
    }

    refreshUser() {
        this.meObserver.next('');
    }

    public subscribeToCurrentUser() {
        this.subscription = this._me.subscribe();
    }

    getMe() {
        return this.httpService.get<UserDetailInterface>('me');
    }

    getMeAsObservable() {
        return this.pipeMyObservable(this.httpService.get<UserDetailInterface>('me', {}));
    }

    private pipeMyObservable(observable: Observable<UserDetailInterface>): Observable<UserDetailInterface> {
        return observable.pipe(
            switchMap(() => this.getMe()),
            map((user) => {
                AuthStorage.setCurrentUser(user);
                return user;
            }),
            shareReplay(10),
        );
    }
}

/**
 * External API response message/code
 */
export enum HttpResponseStatus {
    SUCCESS = 0,
    AUTHENTICATION_FAILURE = 1,
    INSUFFICIENT_USER_RIGHTS = 2,
    MALFORMED_REQUEST = 3,
    MISSING_PARAMETERS = 4,
    BAD_FIELD_VALUE = 5,
    INCONSISTENCY_ERROR = 6,
    LICENSE_RESTRICTION = 8,
    CUSTOMER_NOT_FOUND = 20,
    SYSTEM_ERROR = 21,
    TIMEOUT = 22,
    THROTTLING = 23,
    THIRD_PARTY_ERROR = 24,
    LOCK_ERROR = 25,
    CUSTOMER_ALREADY_EXISTS = 50,
    CUSTOMER_NOT_ELIGIBLE = 51,
    ELEMENT_NOT_FOUND = 80,
    ALREADY_APPROVED = 81,
    APPROBATION_EXPIRED = 82,
    ALREADY_DISAPPROVED = 83,
    APPROBATION_CANCELED = 84,
    ALREADY_REQUESTED = 85,
    OBSOLETE_APPROBATION = 86,
    BONUS_NOT_FOUND = 100,
    INSUFFICIENT_BONUS_BALANCE = 101,
    CAMPAIGN_NOT_FOUND = 200,
    CAMPAIGN_BAD_STATUS = 201,
    CUSTOMER_ALREADY_IN_CAMPAI = 202,
    LOYALTY_PROJECT_NOT_FOUND = 300,
    CUSTOMER_NOT_IN_PROJECT = 301,
    LOYALTY_CLASS_NOT_IN_PROJECT = 302,
    OFFER_NOT_FOUND = 400,
    PRODUCT_NOT_FOUND = 401,
    INVALID_PRODUCT = 402,
    OFFER_NOT_APPLICABLE = 403,
    INSUFFICIENT_STOCK = 404,
    INSUFFICIENT_BALANCE = 405,
    BAD_OFFER_STATUS = 406,
    PRICE_NOT_APPLICABLE = 407,
    NO_VOUCHER_CODE_AVAILABLE = 408,
    CHANNEL_DEACTIVATED = 409,
    CUSTOMER_OFFER_LIMIT_REACHED = 410,
    BAD_OFFER_DATES = 411,
    CHANNEL_NOT_FOUND = 412,
    VOUCHER_CODE_NOT_FOUND = 413,
    PARTNER_NOT_FOUND = 414,
    VOUCHER_ALREADY_REDEEMED = 415,
    VOUCHER_EXPIRED = 416,
    VOUCHER_NOT_ASSIGNED = 417,
    VOUCHER_NON_REDEEMABLE = 418,
    INVALID_TOKEN_CODE = 500,
    MSISDN_TOKEN_CODE_NOT_COM = 501,
    CONCURRENT_ALLOCATION = 502,
    CONCURRENT_ACCEPT = 503,
    NO_OFFER_ALLOCATED = 504,
    NO_OFFER_ACCEPTED = 505,
    NO_OFFER_REFUSED = 506,
    NO_TOKENS_RETURNED = 507,
    NO_OFFERS_RETURNED = 508,
    TOO_MANY_ACCEPTED_OFFERS = 509,
    TOKEN_RESEND_NO_ACTIVE_TOKE = 510,
    TOKEN_RESEND_MAX_NUMBER_OF_RESEND_FOR_TOKEN = 511,
    TOKEN_RESEND_MESSAGE_SENDING_ERROR = 512,
    CHANNEL_WITH_ALLOCATED_OFFERS = 513,
    INVALID_STRATEGY = 517,
    PREDICTION_NOT_AVAILABLE = 600,
    NO_CUSTOMER_LANGUAGE = 701,
    NO_CUSTOMER_CHANNEL = 702,
    UNDELIVERABLE = 703,
    INVALID = 704,
    QUEUE_FULL = 705
}

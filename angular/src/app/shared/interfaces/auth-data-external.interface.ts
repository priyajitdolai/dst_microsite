export interface AuthDataExternalInterface {
    apiVersion: number;
    resellerDetails:
        {
            resellerActive: string;
            resellerDisplay: string;
            resellerCreatedDate: string;
            resellerUpdatedDate: string;
            resellerUserIDs: [];
            resellerName: string;
            resellerParentID: string;
            resellerEmail: string;
            resellerDescription: string;
            resellerWebsite: string;
        };
    description: string;
    responseMessage: string;
    responseCode: number;
}

export interface ExternalVoucherDetailInterface {
    id?: number;
    voucherCode?: string;
    redeemDate?: any;
    msisdn?: number;
    offerName?: string;
    offerDisplay?: string;
    supplier: any;
    offerDetails?: {};
    description?: string;
    responseMessage?: string;
    responseCode?: number;
    expiryDate?: any;
    alreadyRedeemed?: boolean;
}

import { AbstractPersonInterface } from '@app/shared/interfaces/abstract-person.interface';

export interface UserDetailInterface extends AbstractPersonInterface {
    email: string;
    enabled: boolean;
}

import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, shareReplay } from 'rxjs/operators';
import { SecurityService } from '@services/security.service';
import { UserDetailInterface } from '@app/shared/interfaces/user-detail.interface';
import { AuthStorage } from '@app/utils/auth-storage';

@Injectable()
export class AuthenticatedUserResolver implements Resolve<any> {

    constructor(
        protected router: Router,
        protected securityService: SecurityService,
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const currentUser = AuthStorage.getCurrentUser();
        if (currentUser) {
            return of(currentUser);
        }

        return this.securityService.getMeAsObservable().pipe(
            catchError(() => {
                return of(null);
            }),
            map((user: UserDetailInterface) => {
                if (user) {
                    return user;
                }

                this.router.navigate(['/', 'login']);
                return false;
            }),
            shareReplay(5),
        );
    }
}

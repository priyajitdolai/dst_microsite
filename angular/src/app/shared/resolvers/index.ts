import { Type } from '@angular/core';
import { AuthenticatedUserResolver } from '@app/shared/resolvers/authenticated-user.resolver';

export const resolvers: Type<any>[] = [
    AuthenticatedUserResolver,
];

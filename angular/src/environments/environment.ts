// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: true,
    defaultLang: 'en',
    baseUrl: 'http://dst.loc',
    externalApiUrl: 'https://uat-api-gw.dst.com.bn',
    ssoJwtToken: 'eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJjMmRmZDQyOC03ZDI4LTQ4ODAtODMxOS00MTZjODkzYjcwMGIiLCJhdWQiOiJodHRwOi8vYXBpc2VydmVyL2FwaS9vYXV0aC90b2tlbiIsImV4cCI6Ijk5OTk5OTk5OTkiLCJpYXQiOiIxMzQxMzU0MzA1In0.GpDsB8RkL3Tep1NBHEENLtmgvHNOIsMO2xGmCLSbCuoGYMrwYXCfPZR-N7qPSRRK53hZuE6tmSqHS19Sa89cOqvf5XhuB_Ov7Rg9E1uU1wUktkRDUED5tSN8NCOpOhYtpBJaPHaKAZ-L33FFNNyanXtN9fhTalsx5NNFY8tXHGWga_JyA2GQq-ZyNLTXG0ctkTd08bmgmI5K8Pibjthat3vJIt86lrKYBu5lW_WwTfp1xykUVvW0pZXEVOQF7_d86FYdcD8rXp-p0HngIUrWAC5A6K3Lmgcgp44cyfVIasot2d2t6swAmOBXUTpSfmXSPnhtSnhHyxXJt_RIrIVuKQ',
    useExternalApi: true, //true to use the external DST API
    apiVersion: 1,
    // recaptchaKey: '6Leh384ZAAAAADZPcCc_NMyYQUXFS1-6_BhXNJmJ', // old value
    recaptchaKey: '6LfwDIQcAAAAAI6thYeMKFi_BsbI9ZP9c5Ya5mN-',
    // recaptchaSecret: '6Leh384ZAAAAACnUZHjkLFFOF4UWYzX8Mbl1he8U', // old value
    recaptchaSecret: '6LfwDIQcAAAAAHSd5K6W2r3TOmCQFumw2bb-l_HC',
    showLoginRecaptcha: true,
    showCheckCodeRecaptcha: false,
    logUserActions: false,
    enablePhoneCodeFlag: false,
    phoneCodes: [],
    EXPIRED_VOUCHER_ALLOW_REDEEM: false,
    suppliers: [
        {id: 'dummy', name: 'Dummy Supplier'},
        {id: '33', name: 'McDonald\'s'},
        {id: '44', name: 'Citroën'},
        {id: 'supplier_01', name: 'supplier_01'},
    ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

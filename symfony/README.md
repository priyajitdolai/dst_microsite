---
API project requirements:
- php-fpm: 7.3 with (https://www.php.net/manual/en/book.image.php)
- nginx: 1.17 (min) OR apache: 2.4.x
- mysql: Ver 15.1 Distrib 10.3.23-MariaDB
- nodejs v.10.23.2 + npm v6.14.1
- for more info: https://symfony.com/doc/4.2/reference/requirements.html

---
FRONT project requirements:
- nodejs v.10.23.2 + npm v6.14.1
- angular CLI v.9 (`npm i @angular/cli@9`)

---

To install project  follow these instructions:
---
### Standalone mode API:

1. Clone project from repository (develop branch)

2. Move in project folder
   ```cd dst/symfony```

3. Project contains 3 env files:

    .env - default for development
    
    .env.prod - production
    
    .env.test - test
    
    If you use your own php docker container or another config you must set env variables for production
    ```
    ARG APP_ENV: prod
    ARG APP_DEBUG: false
    ```
    

4. Edit .env file with your parameters
    `MAILER_DSN` smtp mail server
    `DATABASE_URL` url to mysql server 
    `GOOGLE_RECAPTCHA_SITE_KEY` create by (https://www.google.com/recaptcha/admin/create)
    `GOOGLE_RECAPTCHA_SECRET_KEY` create by (https://www.google.com/recaptcha/admin/create)
    `CORS_ALLOW_ORIGIN` front application domains that can access the standalone API
    ```
    Example: for `http://app.dst.loc`
    Will be `^https?://app.dst.loc(:[0-9]+)?$`
    ```
5. In file: /symfony/config/services.yaml edit params:
    ```
    defaultFrom: "dst@mail.com"
    deleteFilesOlderThanNdays: 7
    ```
6. Run command to install packages `composer install --no-dev --optimize-autoloader`

7. Generate jwt keys by run following commands (passphrase should be minimum 5 characters long):
    ```
    openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
    openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
    chmod 777 -R config/jwt/*
    ```
8. Check `.env.prod` file to contain correct passphrase you entered while generated keys by previous commands
      
9. Run update database schema command `bin/console doctrine:schema:update --force`

10. Server nginx configuration can be found in `dst/docker/nginx/symfony.conf`

11. Create cron for every of these commands:

    `bash symfony/import_vouchers.sh  &>> ../csv_files/import_logs/vouchers/logs` (Import vouchers (first edit params in file import_vouchers.sh))
    
    `bash symfony/import_users.sh  &>> ../csv_files/import_logs/users/logs` (Import users (first edit params in file import_users.sh))
    
    `symfony/bin/console app:export:users-actions` (export user actions)
            
    `symfony/bin/console app:export:redemption` (export redemption)


12. Project customization 

    * Edit colors and font-family in `symfony/assets/css/app.scss`
    * Edit logo: replace images in `symfony/assets/images`
    * Edit texts translation files `symfony/translations`
    
    After customization run command `composer install`

---
### FRONT APPLICATION:
---

1. Add you configs in `environment.prod.ts` and `environment.prodext.ts` files

`environment.prod.ts` - for standalone API mode
`environment.prodext.ts` - for Evolution API mode


2. To add new phone country code in "Validate voucher" form:

    Add new object in "phoneCodes" array
    
    Example: If we have only ``Afghanistan`` phone country code: 
    ```
    phoneCodes: [
        {
            "name": "Afghanistan",
            "isoCode": "AF",
            "phoneCode": "93"
        }
    ]
    ``` 
    
    And if we also want to add ``Albania`` phone country code, the configuration will become:
    ```
    phoneCodes: [
        {
            "name": "Afghanistan",
            "isoCode": "AF",
            "phoneCode": "93"
        },
        {
            "name": "Albania",
            "isoCode": "AL",
            "phoneCode": "355"
        },
    ]
    ``` 
    
    Sites to get information about phone country and ISO (two letters ISO2 format) codes:
    
   ```
    https://countrycode.org/`
    https://simple.wikipedia.org/wiki/List_of_country_calling_codes
   ```

3. To enable country flags near phone country code set value ``enablePhoneCodeFlag: true``

    Note:
    If ``phoneCodes`` not set, phone country code will be only FR by default

4 Suppliers in "Validate voucher" form:

    Array of objects in "suppliers" property in src/environments/environment.ts file.
    Where: id - the value that will be send by the form, name - displayed value in UI
    
    Example:
    ```
    suppliers: [
        {id: '1', name: 'McDonald\'s'},
        {id: '2', name: 'Citroën'}
    ]
    ```
    
Projects specific:
-
Evolving API mode `environment.prodext.ts`:

1. `baseUrl` - the standalone API url (not need if logUserActions = false)
2. `recaptchaKey` and `recaptchaSecret` create by (https://www.google.com/recaptcha/admin/create)
3. `externalApiUrl` - Evolving API domain

Standalone API mode `environment.prod.ts`:

1. `baseUrl` - the standalone API url
2. `recaptchaKey` and `recaptchaSecret` create by (https://www.google.com/recaptcha/admin/create)


Build application:
-
``cd angular``
``npm i``

Evolving API mode:
`ng build --configuration="productionext"`

Standalone API mode:
`ng build --configuration="production"`

Server nginx configuration can be found in `dst/docker/nginx/angular.conf`

In case docker is available:

#production
`docker-compose build`
`docker-compose up -d`

`docker-compose exec php-fpm composer install --no-dev --optimize-autoloader`
`docker-compose exec php-fpm bin/console doctrine:schema:create`

`docker-compose exec php-fpm openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096`
`docker-compose exec php-fpm openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout`
`docker-compose exec php-fpm chmod 777 -R config/jwt/*`
    
`docker-compose exec db bash import_vouchers.sh  &>> ../csv_files/import_logs/vouchers/logs` (Import vouchers (first edit params in file import_vouchers.sh))
`docker-compose exec php-fpm bash import_users.sh  &>> ../csv_files/import_logs/users/logs` (Import users (first edit params in file import_users.sh))
`docker-compose exec php-fpm bin/console app:export:users-actions` (export user actions)
`docker-compose exec php-fpm bin/console app:export:redemption` (export redemption)

#dev
`docker-compose -f docker-compose.yml -f docker-compose-dev.yml build`
`docker-compose -f docker-compose.yml -f docker-compose-dev.yml up`
`docker-compose exec php-fpm composer install`

Front app will be up on port 4200
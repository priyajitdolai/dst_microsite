import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from '@pages/login/login-page.component';
import { RootPageComponent } from '@pages/root/root-page.component';
import { IsAuthenticatedAnonymouslyGuard } from '@app/shared/guards/is-authenticated-anonymously.guard';
import { IsFullyAuthenticatedGuard } from '@app/shared/guards/is-fully-authenticated.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [IsFullyAuthenticatedGuard],
        children: [
            {
                path: '',
                component: RootPageComponent,
            },
            {
                path: 'dashboard',
                component: RootPageComponent,
            },
        ]
    },
    {
        path: 'login',
        component: LoginPageComponent,
        canActivate: [IsAuthenticatedAnonymouslyGuard]
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: '',
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@environments/environment';
import { StorageHelper } from '@helpers/storage.helper';
import { SecurityService } from '@services/security.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {

    constructor(
        private translateService: TranslateService,
        private securityService: SecurityService,
    ) {
        translateService.setDefaultLang(environment.defaultLang);

        translateService.use(environment.defaultLang);
        const loc = window.location.toString();
        if (loc.startsWith('http://localhost') || loc.startsWith('https://localhost')) {
            environment.showLoginRecaptcha = false;
            environment.showCheckCodeRecaptcha = false;
        }
    }

    ngOnInit() {
        this.securityService.subscribeToCurrentUser();
    }

    ngOnDestroy() {
        StorageHelper.clearAttempts();
    }
}

import {
    Component,
    EventEmitter,
    OnInit,
    Output,
    ElementRef,
    Renderer2,
    ViewChild,
    OnDestroy,
    AfterViewInit
} from '@angular/core';
import { HttpResponseStatus } from '@app/shared/enum/http-response-status.enum';
import { LogAction } from '@app/shared/enum/log-action.enum';
import { AuthStorage } from '@app/utils/auth-storage';
import { HttpService } from '@services/http.service';
import { LoaderService } from '@services/loader.service';
import { SubscriptionComponent } from '@components/subscription.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VoucherService } from '@services/voucher.service';
import { VoucherDetailInterface } from '@app/shared/interfaces/voucher-detail.interface';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { environment } from '@environments/environment';
import { Endpoint } from '@app/shared/enum/endpoint.enum';
import { LogActionInterface } from '@app/shared/interfaces/log-action.interface';

declare var $: any;

@Component({
    selector: 'app-check-code',
    templateUrl: './check-code.component.html',
    styleUrls: ['./check-code.component.scss'],
})
export class CheckCodeComponent extends SubscriptionComponent implements OnInit, OnDestroy, AfterViewInit {
    @Output() getVoucher: EventEmitter<VoucherDetailInterface> = new EventEmitter<VoucherDetailInterface>();
    @Output() getFormData: EventEmitter<any> = new EventEmitter<any>();
    @Output() resetForm: EventEmitter<any> = new EventEmitter<any>();

    form: FormGroup;
    voucher: VoucherDetailInterface;

    public enablePhoneCodeFlag = environment.enablePhoneCodeFlag;
    private unlistenMouseMove: () => void;
    private optionIds: any = [];

    public defaultPhoneCode = {
        name: 'France',
        isoCode: 'FR',
        phoneCode: '33'
    };

    public defaultSelectedPhoneCode;

    public showRedeem = false;
    public suppliers: any = environment.suppliers;
    public phoneCodes: any = [];
    public missingMandatory = false;

    @ViewChild('select', {static: true}) selectElRef: ElementRef;

    constructor(
        private loaderService: LoaderService,
        private fb: FormBuilder,
        private voucherService: VoucherService,
        private translateService: TranslateService,
        private captchaService: ReCaptchaV3Service,
        private httpService: HttpService,
        private renderer: Renderer2
    ) {
        super();
    }

    ngOnInit() {
        this.initForm();
    }

    ngAfterViewInit() {

        if (environment.phoneCodes && environment.phoneCodes.length > 0) {
            const selectEl = this.selectElRef;
            let items = selectEl['itemsList']['items'];
            if (items) {
                items.forEach((item) => {
                    this.optionIds.push(item.htmlId)
                });
            }

            this.unlistenMouseMove = this.renderer.listen('document', 'mousemove', event => {
                if (this.optionIds.includes(event.target.id)) {
                    $('#' + event.target.id + ' .tooltip-wrapper').tooltip();
                }
            });
        }

    }

    onClose() {
        if (this.optionIds.length > 0 && environment.phoneCodes && environment.phoneCodes.length > 0) {
            this.optionIds.forEach((item) => {
                $('#' + item + ' .tooltip-wrapper').tooltip('hide');
            });
        }
    }

    initForm() {
        if (!environment.phoneCodes || environment.phoneCodes.length === 0) {
            this.phoneCodes = [this.defaultPhoneCode]
        } else {
            this.phoneCodes = environment.phoneCodes;
        }

        this.defaultSelectedPhoneCode = this.phoneCodes[0]['phoneCode'];

        this.form = this.fb.group({
            msisdn: this.fb.control(null, []),
            voucherCode: this.fb.control(null, [Validators.required]),
            supplier: this.fb.control('', (environment.useExternalApi ? [Validators.required] : [])),
            phoneCode: this.fb.control(this.defaultSelectedPhoneCode),
        });
    }

    clearRedeem() {
        this.showRedeem = false;
	this.missingMandatory= false;
        this.resetForm.emit(true);
    }

    search() {
        if (!this.form.valid) {
            this.form.markAllAsTouched();
	    this.missingMandatory= true;
            return;
        }

        this.clearRedeem();
        const value = this.form.value;
        let action: Observable<any>;
        if (environment.showCheckCodeRecaptcha) {
            action = this.captchaService.execute('login').pipe(
                switchMap((token) => {
                    // TODO log the token ? use it ?
                    return this.voucherSearch(value);
                })
            );
        } else {
            action = this.voucherSearch(value);
        }

        this.subscriptions = action.subscribe((voucher) => {

            if (environment.useExternalApi) {
                if (voucher.status === 'ok') {
                    voucher = voucher.result;
                } else {
                    voucher = {
                        responseCode: +voucher.errors[0].code,
                        responseMessage: voucher.errors[0].message
                    };
                }
            }

            if (environment.logUserActions) {

                let values = {voucherCode: this.form.value.voucherCode};
                if (this.form.value.msisdn && this.form.value.msisdn.length > 0) {
                    values['msisdn'] = this.form.value.msisdn;
                }

                let status = voucher.responseCode !== HttpResponseStatus.SUCCESS ? LogAction.STATUS_ERROR : LogAction.STATUS_SUCCESS;
                let userName = AuthStorage.getCurrentExternalUser() ? AuthStorage.getCurrentExternalUser().email : '';
                let logAction: LogActionInterface = {
                    username: userName,
                    action: LogAction.ACTION_CHECK_CODE,
                    status: status,
                    values: values
                };

                this.httpService.post(Endpoint.actionLog, logAction).subscribe();
            }
            if (voucher && voucher.hasOwnProperty('expiryDate') && !environment.useExternalApi) {
                voucher['expiryDate'] = voucher.expiryDate.date;
            }

            this.voucher = voucher;
            this.getFormData.emit(value);
            this.getVoucher.emit(voucher);
            this.resetForm.emit(false);
        });
    }

    voucherSearch(value): Observable<VoucherDetailInterface> {
        return this.voucherService.search(value).pipe(
            catchError((err) => {
                return of({} as any);
            })
        );
    }

    clear() {
        this.form.patchValue({voucherCode: null, msisdn: null, supplier: '', phoneCode: ''});
        this.form.markAsPristine();
	this.missingMandatory= false;
	this.resetForm.emit(true);
    }

    submit() {
        if (!this.form.valid) {
            this.form.markAllAsTouched();
	    this.missingMandatory= true;
            return;
        }
    }

    get supplier() {
        return this.form.get('supplier');
    }

    changeSupplier(e) {
        this.supplier.setValue(e.target.value, {
            onlySelf: true
        });
    }

    ngOnDestroy() {
        if (this.unlistenMouseMove) {
            this.unlistenMouseMove();
        }
    }
}

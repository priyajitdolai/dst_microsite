import { Component } from '@angular/core';

@Component({
    selector: 'app-clear',
    template: '<div style="clear: both"></div>',
})
export class ClearComponent {
}

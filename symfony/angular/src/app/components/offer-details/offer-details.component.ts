import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {VoucherDetailInterface} from '@app/shared/interfaces/voucher-detail.interface';
import { environment } from '@environments/environment';
import * as moment from 'moment';

@Component({
    selector: 'app-offer-details',
    templateUrl: './offer-details.component.html',
    styleUrls: ['./offer-details.component.scss'],
})
export class OfferDetailsComponent implements OnInit {

    @Input() voucher: VoucherDetailInterface;
    form: FormGroup;

    constructor() {
    }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
    }

    get unavailable(): boolean {
        return !this.voucher || (!this.voucher.offerName && !this.voucher.offerDisplay) || this.voucher.redeemDate;
    }

    get notExpired() {
        let isNotExpired = false;
        if (
            this.voucher &&
            (
                !this.voucher.expiryDate ||
                moment(this.voucher.expiryDate) > moment() ||
                (environment.EXPIRED_VOUCHER_ALLOW_REDEEM && !environment.useExternalApi)
            )
        ) {
            isNotExpired = true;
        }
        return isNotExpired;
    }
}

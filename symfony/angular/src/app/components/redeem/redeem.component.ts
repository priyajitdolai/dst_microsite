import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { LogAction } from '@app/shared/enum/log-action.enum';
import { LogActionInterface } from '@app/shared/interfaces/log-action.interface';
import { HttpService } from '@services/http.service';
import {LoaderService} from '@services/loader.service';
import {SubscriptionComponent} from '@components/subscription.component';
import {VoucherDetailInterface} from '@app/shared/interfaces/voucher-detail.interface';
import {Status} from '@app/shared/enum/voucher-status.enum';
import {VoucherService} from '@services/voucher.service';
import * as moment from 'moment';
import { environment } from '@environments/environment';
import { HttpResponseStatus } from '@app/shared/enum/http-response-status.enum';
import { AuthStorage } from '@app/utils/auth-storage';
import { Endpoint } from '@app/shared/enum/endpoint.enum';

@Component({
    selector: 'app-redeem',
    templateUrl: './redeem.component.html',
    styleUrls: ['./redeem.component.scss'],
})
export class RedeemComponent extends SubscriptionComponent implements OnInit {

    @Input() voucher: VoucherDetailInterface;
    @Input() formData: any;
    @Input() resetData: boolean;
    @Output() voucherChange: EventEmitter<VoucherDetailInterface> = new EventEmitter<VoucherDetailInterface>();
    // tslint:disable-next-line:variable-name
    readonly statuses = Status;
    submitted = false;

    constructor(
        private loaderService: LoaderService,
        private voucherService: VoucherService,
        private httpService: HttpService
    ) {
        super();
    }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.submitted = false;
    }

    redeem() {
        this.submitted = false;
        this.voucherService.redeem(this.voucher.voucherCode, this.formData).subscribe((response) => {

            let status = LogAction.STATUS_ERROR;
            if (environment.useExternalApi) {
                if (response.status === 'ok') {
                    response = response.result;
                } else {
                    response = {
                        responseCode: +response.errors[0].code,
                        responseMessage: response.errors[0].message
                    };
                }

                if (response.responseCode === HttpResponseStatus.SUCCESS) {
                    this.voucher = {
                        id: response.offerDetails.offerID,
                        voucherCode: this.formData.voucherCode,
                        redeemDate: new Date(),
                        msisdn: this.formData.msisdn,
                        expiryDate: response.expiryDate,
                        offerName: response.offerDetails.offerName,
                        offerDisplay: response.offerDetails.offerDisplay,
                        supplier: this.formData.supplier,
                    };

                    status = LogAction.STATUS_SUCCESS;
                } else {
                    this.voucher = response;
                }

                if (environment.logUserActions) {
                    let values = this.formData;
                    values.voucherCode = this.voucher.voucherCode;
                    let userName = AuthStorage.getCurrentExternalUser() ? AuthStorage.getCurrentExternalUser().email : '';
                    let logAction: LogActionInterface = {
                        username: userName,
                        action: LogAction.ACTION_REDEEM_CODE,
                        status: status,
                        values: values
                    };
                    this.httpService.post(Endpoint.actionLog, logAction).subscribe();
                }

            } else {
                this.voucher = response;
            }

            this.voucherChange.emit(this.voucher);
            this.submitted = true;
        });
    }

    get status() {
        return this.voucher && this.voucher.redeemDate ? this.statuses.success : this.statuses.error;
    }

    get notExpired() {
        return this.voucher && (!this.voucher.expiryDate || moment(this.voucher.expiryDate) > moment());
    }

    get showRedeemButton() {
        let isAvailable = false;
        if (
            this.voucher?.id && !this.voucher?.redeemDate && !this.submitted &&
            (this.notExpired || (environment.EXPIRED_VOUCHER_ALLOW_REDEEM && !environment.useExternalApi))
        ) {
            isAvailable = true;
        }

        return isAvailable;
    }
}

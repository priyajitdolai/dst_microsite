import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

export class SubscriptionComponent implements OnDestroy {

    private subscriptionBackPack: Subscription[] = [];

    protected set subscriptions(value: Subscription) {
        this.subscriptionBackPack.push(value);
    }

    constructor() {
        this.subscriptionBackPack = [];
    }

    ngOnDestroy() {
        this.unsubscribeEvents();
    }

    unsubscribeEvents() {
        this.subscriptionBackPack.forEach(
            sub => sub.unsubscribe()
        );
        this.subscriptionBackPack = [];
    }

}

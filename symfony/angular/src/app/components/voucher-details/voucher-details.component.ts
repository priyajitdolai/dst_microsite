import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { VoucherDetailInterface } from '@app/shared/interfaces/voucher-detail.interface';
import { environment } from '@environments/environment';
import * as moment from 'moment';
import { HttpResponseStatus } from '@app/shared/enum/http-response-status.enum';

@Component({
    selector: 'app-voucher-details',
    templateUrl: './voucher-details.component.html',
    styleUrls: ['./voucher-details.component.scss'],
})
export class VoucherDetailsComponent {

    @Input() voucher: VoucherDetailInterface;
    form: FormGroup;

    constructor() {
    }

    get customerNotFound() {
        return this.voucher.responseCode == HttpResponseStatus.CUSTOMER_NOT_FOUND;
    }

    get notExpired() {
        let isNotExpired = false;

        if (
            this.voucher &&
            (
                !this.voucher.expiryDate ||
                moment(this.voucher.expiryDate) > moment() ||
                (environment.EXPIRED_VOUCHER_ALLOW_REDEEM && !environment.useExternalApi)
            )
        ) {
            isNotExpired = true;
        }

        return isNotExpired;
    }
}

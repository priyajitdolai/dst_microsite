import { Observable, Subject } from 'rxjs';

export class StateHolder {

    protected states: any = {};
    protected eventStream: Subject<any> = new Subject();
    readonly stateChange: Observable<any> = this.eventStream.asObservable();

    constructor(states?: any) {
        if (states) {
            this.defineStates(states, false);
        }
    }

    isEnabled(name): boolean {
        return !!this.get(name);
    }

    isDisabled(name): boolean {
        return !this.get(name);
    }

    get(name): boolean | number {
        if (!(name in this.states)) {
            throw new Error(`state not defined, given ${name}`);
        }

        return this.states[name];
    }

    set(name, value: boolean | number) {
        if (!(name in this.states)) {
            throw new Error(`state not defined, given ${name}`);
        }

        this.states[name] = value;
        this.eventStream.next(name);
    }

    enable(name) {
        this.set(name, true);
    }

    disable(name) {
        this.set(name, false);
    }

    inc(name) {
        let val: number = this.get(name) as number;
        const nVal = isNaN(val) ? (val ? 1 : 0) : ++val;
        this.set(name, nVal);
    }

    dec(name) {
        let val: number = this.get(name) as number;
        const nVal = isNaN(val) ? 0 : --val;
        this.set(name, nVal <= 0 ? 0 : nVal);
    }

    toggle(name) {
        this.set(name, !this.get(name));
    }

    defineState(name, defaultValue: boolean = false) {
        if (name in this.states) {
            throw new Error(`state already defined, given ${name}`);
        }

        this.states[name] = defaultValue;
    }

    defineStates(states: any, merge: boolean = true) {
        const base = merge ? this.states : {};
        this.states = Object.assign(base, states);
    }

    dump() {
        return Object.assign({}, this.states);
    }

}

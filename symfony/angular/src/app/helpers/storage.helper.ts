import * as moment from 'moment';

export class StorageHelper {
    private static readonly LOGIN_ATTEMPTS_COUNTER = '_dst_LAC_';
    private static readonly LOGIN_BLOCKED_TILL = '_dst_LBV_';
    private static readonly LAST_LOGIN_ATTEMPT = '_dst_LLA_';

    public static loginAttempted() {
        const lac = StorageHelper.getCountAttempts() + 1;
        localStorage.setItem(StorageHelper.LOGIN_ATTEMPTS_COUNTER, JSON.stringify(lac));
        localStorage.setItem(StorageHelper.LAST_LOGIN_ATTEMPT, JSON.stringify(moment().format()));
    }

    public static getCountAttempts(): number {
        StorageHelper.checkLastAttempt();
        const lac: any = localStorage.getItem(StorageHelper.LOGIN_ATTEMPTS_COUNTER);
        return lac ? Number(JSON.parse(lac)) : 0;
    }

    public static checkLastAttempt() {
        if (StorageHelper.getBlockedValidity()) {
            return;
        }

        let lastLoginAttempt = localStorage.getItem(StorageHelper.LAST_LOGIN_ATTEMPT);
        if (lastLoginAttempt) {
            lastLoginAttempt = JSON.parse(lastLoginAttempt);
            if (moment().diff(moment(lastLoginAttempt).add(5, 'minutes')) > 0) {
                StorageHelper.clearAttempts();
            }
        }
    }

    public static clearAttempts() {
        localStorage.removeItem(StorageHelper.LOGIN_ATTEMPTS_COUNTER);
    }

    public static getBlockedValidity(): Date | null {
        let storageDate: any = localStorage.getItem(StorageHelper.LOGIN_BLOCKED_TILL);
        if (storageDate) {
            storageDate = JSON.parse(storageDate);
            return new Date(storageDate);
        }

        return null;
    }

    public static setBlockedValidity(till: string | null): void {
        if (!till) {
            localStorage.removeItem(StorageHelper.LOGIN_BLOCKED_TILL);
        } else {
            localStorage.setItem(StorageHelper.LOGIN_BLOCKED_TILL, JSON.stringify(till));
        }
    }
}

export class StringHelper {
    public static capitalize(value: string) {
        if (!value.length) {
            return '';
        }

        const parts = value.split(' ');

        return parts.map(part => StringHelper.ucfirst(part)).join(' ');
    }

    public static ucfirst(value: string) {
        value = value.trim();
        if (!value.length) {
            return '';
        }

        return value[0].toUpperCase() + value.substr(1);
    }
}

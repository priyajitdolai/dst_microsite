import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { StorageHelper } from '@helpers/storage.helper';
import { interval, Observable, ReplaySubject, Subject } from 'rxjs';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { SubscriptionComponent } from '@components/subscription.component';

@Component({
    selector: 'app-login-blocked-page',
    templateUrl: './login-blocked-page.component.html',
    styleUrls: ['./login-blocked-page.component.scss'],
})
export class LoginBlockedPageComponent extends SubscriptionComponent implements OnInit {
    minutesObserver: Subject<any> = new ReplaySubject();
    minutes$: Observable<any> = this.minutesObserver.pipe(
        switchMap(() => interval(1000)),
        map(() => this.getBlockedMinutes()),
        shareReplay(),
    );
    secondsObserver: Subject<any> = new ReplaySubject();
    seconds$: Observable<any> = this.secondsObserver.pipe(
        switchMap(() => interval(1000)),
        map(() => this.getBlockedSeconds()),
        shareReplay(),
    );

    constructor(
    ) {
        super();
        this.secondsObserver.next('');
        this.minutesObserver.next('');
    }

    ngOnInit() {
    }

    getBlockedSeconds() {
        let seconds = moment(StorageHelper.getBlockedValidity()).diff(moment(), 'seconds');
        while (seconds > 60) {
            seconds -= 60;
        }

        return seconds;
    }

    getBlockedMinutes() {
        return moment(StorageHelper.getBlockedValidity()).diff(moment(), 'minutes');
    }
}

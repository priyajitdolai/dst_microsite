import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Endpoint } from '@app/shared/enum/endpoint.enum';
import { LogAction } from '@app/shared/enum/log-action.enum';
import { LogActionInterface } from '@app/shared/interfaces/log-action.interface';
import { HttpService } from '@services/http.service';
import { SecurityService } from '@services/security.service';
import { Router } from '@angular/router';
import { StorageHelper } from '@helpers/storage.helper';
import * as moment from 'moment';
import { AppParameter } from '@constants/app-parameters';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@environments/environment';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { Subscription } from 'rxjs';
import { LoaderService } from '@services/loader.service';
import { ErrorConst } from '@constants/error';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {
    form: FormGroup;
    public showPassword = false;
    public loginError: string = null;
    private captchaSubscription: Subscription;

    constructor(
        private fb: FormBuilder,
        private securityService: SecurityService,
        private router: Router,
        private translateService: TranslateService,
        private captchaService: ReCaptchaV3Service,
        protected loaderService: LoaderService,
        private httpService: HttpService
    ) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            username: this.fb.control(null, [Validators.required]),
            password: this.fb.control(null, [Validators.required]),
            remember_me: this.fb.control(null, []),
        });
    }

    ngOnDestroy() {
        this.unsubscribe();
    }

    unsubscribe() {
        if (this.captchaSubscription) {
            this.captchaSubscription.unsubscribe();
            this.captchaSubscription = null;
        }
    }

    submit() {
        if (this.form.invalid) {
            this.form.markAllAsTouched();
            this.loginError = this.translateService.instant('error.form.invalid');
            return;
        }

        this.unsubscribe();

        if (environment.showLoginRecaptcha) {
            this.captchaSubscription = this.captchaService.execute('login').subscribe((token) => {
                environment.useExternalApi ? this.externalLoginAction() : this.loginAction();
            }, (err) => {
                // console.log(err);
            });
        } else {
                environment.useExternalApi ? this.externalLoginAction() : this.loginAction();
        }
    }

    loginAction() {
        this.loginError = null;
        return this.securityService.login(this.form.value).catch((err) => {
            this.loaderService.hide();

            let logAction: LogActionInterface = {
                username: this.form.value.username,
                action: LogAction.ACTION_LOGIN,
                status: LogAction.STATUS_ERROR,
                values: {
                    username: this.form.value.username,
                    password: this.form.value.password
                }
            };
            this.httpService.post(Endpoint.actionLog, logAction).subscribe();

            this.form.get('password').setValue(null);

            StorageHelper.loginAttempted();
            this.loginError = this.translateService.instant('error.login_error');
        });
    }

    externalLoginAction() {
        this.loginError = null;
        return this.securityService.loginInExternalApi(this.form.value).catch((err) => {
            this.loaderService.hide();
            if (!environment.production) {
                // console.error(err);
            }

            this.form.get('password').setValue(null);

            if (err === ErrorConst.ssoTokenError) {
                this.loginError = this.translateService.instant('error.sso_token_error');
                return;
            }

            StorageHelper.loginAttempted();
            this.loginError = this.translateService.instant('error.login_error');
        });
    }

    isAllowedLogin() {
        const attempts = StorageHelper.getCountAttempts();
        const blockedDate = StorageHelper.getBlockedValidity();
        if (blockedDate) {
            const diff = moment().diff(moment(blockedDate));
            if (diff < 0) {
                return false;
            } else {
                StorageHelper.setBlockedValidity(null);
                StorageHelper.clearAttempts();
            }
        } else {
            if (attempts >= AppParameter.userLoginAttempts) {
                StorageHelper.setBlockedValidity(moment().add(AppParameter.loginBlockDuration, 'minutes').format());
                return false;
            }
        }

        return true;
    }
}

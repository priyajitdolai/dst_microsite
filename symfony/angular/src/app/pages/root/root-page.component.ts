import { Component, OnInit } from '@angular/core';
import { HttpResponseStatus } from '@app/shared/enum/http-response-status.enum';
import { UserDetailInterface } from '@app/shared/interfaces/user-detail.interface';
import { ExternalVoucher } from '@app/shared/models/external.voucher';
import { AuthStorage } from '@app/utils/auth-storage';
import { Router } from '@angular/router';
import { VoucherDetailInterface } from '@app/shared/interfaces/voucher-detail.interface';
import { environment } from '@environments/environment';

@Component({
    selector: 'app-root-page',
    templateUrl: './root-page.component.html',
    styleUrls: ['./root-page.component.scss']
})
export class RootPageComponent implements OnInit {
    user: UserDetailInterface;
    voucher: VoucherDetailInterface;
    formData;
    voucherRedeemed = false;
    resetData = false;

    constructor(
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.user = AuthStorage.getCurrentUser() as UserDetailInterface;
        this.resetData = false;
    }

    setFormData(data) {
        this.formData = data;
    }

    setVoucher(data) {

        if (environment.useExternalApi) {
	    console.log("parseVoucherData:",data);
            this.voucher = this.parseVoucher(data, this.formData);
        } else {
            this.voucher = data;
        }
    }

    resetForm(data) {
        this.resetData = data;
    }

    updateVoucher(data) {
        if (data && data.redeemDate) {
            this.voucherRedeemed = true;
        }
    }

    logout() {
        AuthStorage.clearData();
        this.router.navigate(['/', 'login']);
    }

    /**
     * Parse voucher and errors from external API
     * @param voucher
     * @param form
     */

    parseVoucherNEW(voucher, form = null) {

        let err = voucher?.errors || [];

        if (err && err[0]) {
                voucher = new ExternalVoucher(
                    null,
                    null,
                    null,
                    voucher.responseDetails?.AlternateIDs?.msisdn,
                    '',
                    '',
                    null,
                    null,
                    null,
                    null,
                    err[0]?.message,
                    err[0]?.code,
                    err[0]?.code == HttpResponseStatus.VOUCHER_ALREADY_REDEEMED,
                );
        }
        else {
                let vc = voucher?.result || {};
                console.log("VC:", vc);
                voucher = new ExternalVoucher(
                    vc.offerDetails.offerID,
                    vc.code,
                    '',
                    this.formData.msisdn,
                    vc.offerDetails.offerDisplay,
                    vc.offerDetails.offerDescription,
                    this.formData.supplier,
                    new Date(vc.expiryDate),
                    vc.offerDetails,
                    vc.offerDetails.offerDescription,
                    vc.responseMessage,
                    vc.responseCode
                );
        }
        return  voucher;
    }

    parseVoucher(voucher, form = null) {

        switch (voucher.responseCode) {
            case HttpResponseStatus.SUCCESS:
                voucher = new ExternalVoucher(
                    voucher.offerDetails.offerID,
                    voucher.code,
                    '',
                    this.formData.msisdn,
                    voucher.offerDetails.offerName,
                    voucher.offerDetails.offerDisplay,
                    this.formData.supplier,
                    new Date(voucher.expiryDate),
                    voucher.offerDetails,
                    voucher.offerDetails.offerDescription
                );
                break;
            default:
                voucher = new ExternalVoucher(
                    null,
                    null,
                    voucher.responseDetails?.RedeemedDate,
                    voucher.responseDetails?.AlternateIDs?.msisdn,
                    '',
                    '',
                    this.formData.supplier,
                    null,
                    null,
                    null,
                    voucher.responseMessage,
                    voucher.responseCode,
                    voucher.responseCode == HttpResponseStatus.VOUCHER_ALREADY_REDEEMED,
                );
                break;
        }
        return voucher;
    }
}

import { Type } from '@angular/core';
import { UcfirstPipe } from '@app/pipes/ucfirst.pipe';
import { Nl2brPipe } from '@app/pipes/nl2br.pipe';

export const pipes: Type<any>[] = [
    UcfirstPipe,
    Nl2brPipe,
];

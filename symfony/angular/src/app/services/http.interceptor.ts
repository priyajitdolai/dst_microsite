import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor as HttpInterceptorInterface, HttpRequest } from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { StateHolder } from '@helpers/state-holder.helper';
import { AuthStorage } from '@app/utils/auth-storage';
import { Router } from '@angular/router';
import { LoaderService } from '@services/loader.service';

@Injectable()
export class HttpInterceptor implements HttpInterceptorInterface {

    protected stateH: StateHolder;
    protected loadStream: Subject<boolean> = new Subject();

    constructor(
        protected loaderService: LoaderService,
        private router: Router,
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError(err => {
                if (err.status === 401 || err.code === 401) { // Unauthorized
                    AuthStorage.clearData();
                    this.router.navigate(['/', 'login']).then(() => {
                        this.loaderService.hide();
                    });
                }

                return throwError(err);
            }),
        );
    }
}

import {Injectable, OnDestroy} from '@angular/core';
import {HttpService} from '@services/http.service';
import {LoaderService} from '@services/loader.service';
import {Router} from '@angular/router';
import {VoucherDetailInterface} from '@app/shared/interfaces/voucher-detail.interface';
import { environment } from '@environments/environment';
import { ExternalVoucherDetailInterface } from '@app/shared/interfaces/external-voucher-detail.interface';
import * as moment from 'moment';
import { AuthStorage } from '@app/utils/auth-storage';

@Injectable()
export class VoucherService implements OnDestroy {

    constructor(
        private httpService: HttpService,
        protected loaderService: LoaderService,
        protected router: Router,
    ) {
    }

    ngOnDestroy() {
    }

    search({voucherCode, msisdn = null, supplier = null, phoneCode = null}) {
        this.loaderService.show();
        const params = {voucherCode} as any;
        if (msisdn && phoneCode && !environment.useExternalApi) {

            let patterns = [
                '00' + phoneCode,
                phoneCode + '0',
                '\\+' + phoneCode,
                phoneCode,
                '0'
            ];

            for (let el of patterns) {
                const regex = RegExp('^' + el);
                if (regex.test(msisdn)) {
                    msisdn = msisdn.replace(regex, '');
                    break;
                }
            }

            params.msisdn = phoneCode + msisdn;
        }

        params.supplier = supplier;

        if (environment.useExternalApi) {
            const supplierObject = environment.suppliers.find(sup => sup.id === supplier);
	    let loginName = AuthStorage.getCurrentExternalUser() ? AuthStorage.getCurrentExternalUser().email : '';
	    let password = AuthStorage.getCurrentExternalUser() ? AuthStorage.getCurrentExternalUser().password : '';
            const body = {
                requestId: `evolving_${moment(new Date()).format('DDMMYYYY')}`,
                data: {
		    loginName,
		    password,
                    apiVersion: environment.apiVersion,
                    msisdn,
                    voucherCode,
                    supplier: supplierObject?.name
                }
            };

            return this.loaderService.loadingPipe(
                this.httpService.post<VoucherDetailInterface>('api/v1/cms/validatevoucher', body, {}, true)
            );
        } else {
            return this.loaderService.loadingPipe(
                this.httpService.get<VoucherDetailInterface>('voucher/search', {params})
            );
        }
    }

    redeem(voucherCode, formData) {
        if (typeof voucherCode === 'object' && voucherCode.hasOwnProperty('voucherCode')) {
            voucherCode = voucherCode.voucherCode;
        }

        this.loaderService.show();

        if (environment.useExternalApi) {
            const supplierObject = environment.suppliers.find(sup => sup.id === formData.supplier);
	    let loginName = AuthStorage.getCurrentExternalUser() ? AuthStorage.getCurrentExternalUser().email : '';
            let password = AuthStorage.getCurrentExternalUser() ? AuthStorage.getCurrentExternalUser().password : '';
            const body = {
                requestId: `evolving_${moment(new Date()).format('DDMMYYYY')}`,
                data: {
		    loginName,
		    password,
                    apiVersion: environment.apiVersion,
                    msisdn: formData.msisdn,
                    voucherCode: formData.voucherCode,
                    supplier: supplierObject?.name,
                    origin: 'origin'
                }
            };

            return this.loaderService.loadingPipe(
                this.httpService.post<ExternalVoucherDetailInterface>('api/v1/cms/redeemvoucher', body, {}, true)
            );
        } else {
            return this.loaderService.loadingPipe(
                this.httpService.post<VoucherDetailInterface>(
                    `voucher/redeem/${voucherCode}`,
                    {redeem: formData}
                )
            );
        }
    }

}

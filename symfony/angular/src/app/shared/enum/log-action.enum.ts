export enum LogAction {
    ACTION_LOGIN = 'login',
    ACTION_CHECK_CODE = 'check_code',
    ACTION_REDEEM_CODE = 'redeem_code',
    STATUS_SUCCESS = 'success',
    STATUS_ERROR = 'error'
}

import { IsAuthenticatedAnonymouslyGuard } from '@app/shared/guards/is-authenticated-anonymously.guard';
import { IsFullyAuthenticatedGuard } from '@app/shared/guards/is-fully-authenticated.guard';

export const guards = [
    IsAuthenticatedAnonymouslyGuard,
    IsFullyAuthenticatedGuard,
];

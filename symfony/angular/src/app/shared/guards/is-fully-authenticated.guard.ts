import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthStorage } from '@app/utils/auth-storage';
import { environment } from '@environments/environment';

@Injectable()
export class IsFullyAuthenticatedGuard implements CanActivate {
    constructor(
        public router: Router
    ) {
    }

    canActivate(): boolean {

        if (!environment.useExternalApi && !AuthStorage.isAuthenticated()) {
            this.router.navigate([ '/', 'login']);
            return false;
        }

        if (environment.useExternalApi && !AuthStorage.isAuthenticatedExternal()) {
            this.router.navigate([ '/', 'login']);
            return false;
        }

        return true;
    }
}

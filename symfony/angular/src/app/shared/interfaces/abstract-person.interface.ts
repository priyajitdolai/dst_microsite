export interface AbstractPersonInterface {
    id: number;
    username: string;
}

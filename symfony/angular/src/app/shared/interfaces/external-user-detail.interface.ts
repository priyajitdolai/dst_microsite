import { AbstractPersonInterface } from '@app/shared/interfaces/abstract-person.interface';

export interface ExternalUserDetailInterface extends AbstractPersonInterface {
    email: string;
    password: string;
    enabled: boolean;
}

export interface VoucherDetailInterface {
    id?: number;
    voucherCode: string;
    salesChannel?: any;
    redeemDate?: any; // date
    redeemUser?: string;
    msisdn?: string;
    expiryDate?: any; // date
    voucherType?: string;
    offerName?: string;
    offerDisplay?: string;
    offerStatus?: string;
    offerId?: number;
    supplier?: any;
    createdAt?: any; // date
    updatedAt?: any; // date
    alreadyRedeemed?: boolean;
    responseCode?: any;

    //external API
    loginName?: string;
    password?: string;
    origin?: string;
    sync?: boolean;
}

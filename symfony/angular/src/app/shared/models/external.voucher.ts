import { ExternalVoucherDetailInterface } from '@app/shared/interfaces/external-voucher-detail.interface';

export class ExternalVoucher implements ExternalVoucherDetailInterface {

    id = null;
    voucherCode = null;
    redeemDate = '';
    msisdn = null;
    offerName = null;
    offerDisplay = null;
    supplier = null;
    offerDetails = null;
    description = null;
    responseMessage = null;
    responseCode = null;
    expiryDate = null;
    alreadyRedeemed = false;

    constructor(
        id = null,
        voucherCode = null,
        redeemDate = null,
        msisdn = null,
        offerName = null,
        offerDisplay = null,
        supplier = null,
        expiryDate = null,
        offerDetails = null,
        description = null,
        responseMessage = null,
        responseCode = null,
        alreadyRedeemed = false,
    ) {
        this.id = id;
        this.voucherCode = voucherCode;
        this.redeemDate = redeemDate;
        this.msisdn = msisdn;
        this.offerName = offerName;
        this.offerDisplay = offerDisplay;
        this.supplier = supplier;
        this.offerDetails = offerDetails;
        this.description = description;
        this.responseMessage = responseMessage;
        this.responseCode = responseCode;
        this.expiryDate = expiryDate;
        this.alreadyRedeemed = alreadyRedeemed;
    }
}

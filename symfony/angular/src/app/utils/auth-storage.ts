import { AuthDataInterface } from '@app/shared/interfaces/auth-data.interface';
import { UserDetailInterface } from '@app/shared/interfaces/user-detail.interface';
import { ExternalUserDetailInterface } from '@app/shared/interfaces/external-user-detail.interface';

export class AuthStorage {

    private static readonly CURRENT_USER_KEY = '_dst_me_';
    private static readonly AUTH_KEY = '_dst_auth_';
    private static readonly IMPERSONATED_KEY = '_dst_impersonated_';

    public static readonly ALREADY_SEE_REGISTRATION_POPUP_KEY = '_registration_popup_';

    public static getAuthData(): AuthDataInterface {
        return (JSON.parse(localStorage.getItem(AuthStorage.AUTH_KEY)) || JSON.parse(sessionStorage.getItem(AuthStorage.AUTH_KEY))) || null;
    }

    public static getAuthDataExternal() {
        let currentUser = this.getCurrentExternalUser();
        return (currentUser && currentUser.email && currentUser.password) || null;
    }

    public static setAuthData(data: AuthDataInterface, rememberMe: boolean = false) {
        AuthStorage.clearAuthData();
        if (rememberMe) {
            localStorage.setItem(AuthStorage.AUTH_KEY, JSON.stringify(data));
            return true;
        }

        sessionStorage.setItem(AuthStorage.AUTH_KEY, JSON.stringify(data));
        return true;
    }

    public static renewAuthData(data: AuthDataInterface) {
        AuthStorage.setAuthData(data, (sessionStorage.getItem(AuthStorage.AUTH_KEY) ? false : true));
    }

    public static clearAuthData() {
        sessionStorage.removeItem(AuthStorage.AUTH_KEY);
        sessionStorage.removeItem(AuthStorage.ALREADY_SEE_REGISTRATION_POPUP_KEY);
        localStorage.removeItem(AuthStorage.AUTH_KEY);
    }

    public static clearData() {
        AuthStorage.clearAuthData();
        AuthStorage.clearImpersonateData();
        AuthStorage.clearCurrentUserData();
    }

    public static getCurrentUser(): UserDetailInterface {
        return JSON.parse(localStorage.getItem(AuthStorage.CURRENT_USER_KEY));
    }

    public static setCurrentUser(user: UserDetailInterface) {
        localStorage.setItem(AuthStorage.CURRENT_USER_KEY, JSON.stringify(user));
    }

    public static setCurrentExternalUser(user: ExternalUserDetailInterface) {
        localStorage.setItem(AuthStorage.CURRENT_USER_KEY, JSON.stringify(user));
    }

    public static getCurrentExternalUser(): ExternalUserDetailInterface {
        return JSON.parse(localStorage.getItem(AuthStorage.CURRENT_USER_KEY));
    }

    public static setCurrentAuthenticatedData(user: UserDetailInterface) {

        if (!user) {
            AuthStorage.clearData();
            return false;
        }

        AuthStorage.setCurrentUser(user);
        return true;
    }

    public static clearCurrentUserData() {
        localStorage.removeItem(AuthStorage.CURRENT_USER_KEY);
    }

    public static clearImpersonateData() {
        localStorage.removeItem(AuthStorage.IMPERSONATED_KEY);
    }

    public static isAuthenticated(): boolean {
        return !!AuthStorage.getAuthData();
    }

    public static isAuthenticatedExternal(): boolean {
        return !!AuthStorage.getAuthDataExternal();
    }

    public static getAuthenticated(): UserDetailInterface {
        return AuthStorage.getCurrentUser() || null;
    }
}

export const environment = {
    production: true,
    defaultLang: 'en',
    externalApiUrl: '',
    apiVersion: 1,
    baseUrl: 'http://dst.loc',
    // recaptchaKey: '6Leh384ZAAAAADZPcCc_NMyYQUXFS1-6_BhXNJmJ', // old value
    recaptchaKey: '6LfwDIQcAAAAAI6thYeMKFi_BsbI9ZP9c5Ya5mN-',
    // recaptchaSecret: '6Leh384ZAAAAACnUZHjkLFFOF4UWYzX8Mbl1he8U', // old value
    recaptchaSecret: '6LfwDIQcAAAAAHSd5K6W2r3TOmCQFumw2bb-l_HC',
    useExternalApi: false,
    ssoJwtToken: '',
    showLoginRecaptcha: true,
    showCheckCodeRecaptcha: false,
    logUserActions: true,
    enablePhoneCodeFlag: true,
    EXPIRED_VOUCHER_ALLOW_REDEEM: true,
    phoneCodes: [
        {
            "name": "Albania",
            "isoCode": "AL",
            "phoneCode": "355"
        }
    ],
    suppliers: [
        {id: '33', name: 'McDonald\'s'},
        {id: '44', name: 'Citroën'},
        {id: 'supplier_01', name: 'supplier_01'},
    ]
};

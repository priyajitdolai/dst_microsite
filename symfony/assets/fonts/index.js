export * from './FontAwesome.otf';
export * from './fontawesome-webfont.woff';
export * from './fontawesome-webfont.woff2';
export * from './fontawesome-webfont.eot';
export * from './fontawesome-webfont.svg';
export * from './fontawesome-webfont.ttf';

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../fonts/index';
import '../css/font-awesome.min.css';
import '../css/app.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';

$(function () {

    $(".reveal").on('click', function () {
        var $pwd = $(".pwd");
        if ($pwd.attr('type') === 'password') {
            $pwd.attr('type', 'text');
        } else {
            $pwd.attr('type', 'password');
        }
    });


    function rescaleCaptcha(){
        var captcha = $('#code_check_form_captcha');
        var width = captcha.width();
        var scale;

        if (width < 302) {
            scale = width / 302;
        } else{
            scale = 1.0;
        }

        captcha.css('transform', 'scale(' + scale + ')');
        captcha.css('-webkit-transform', 'scale(' + scale + ')');
        captcha.css('transform-origin', '0 0');
        captcha.css('-webkit-transform-origin', '0 0');
    }

    rescaleCaptcha();
    $( window ).resize(function() { rescaleCaptcha(); });


});

$(function () {

    $('#modal-filters').on('shown.bs.modal', function (e) {

        let checkExist = setInterval(function () {

            $('#modal-filters .form-widget-compound > .field-datetime [type=\'datetime-local\']').each((i, elem) => {
                let elId = '#' + elem.id;
                if ($(elId).length) {
                    let startDate = $('#' + elem.id);
                    let startDefault = startDate.val() ? moment(startDate.val()) : moment();
                    startDate.attr({
                        'data-target': elId,
                        'data-toggle': 'datetimepicker',
                        'type': 'text',
                        'readonly': 'readonly'
                    });
                    startDate.addClass('custom-datetimepicker');

                    startDate.datetimepicker({
                        format: 'YYYY-MM-DD HH:mm:ss',
                        allowInputToggle: true,
                        // locale: 'en',
                        sideBySide: true,
                        ignoreReadonly: true,
                        buttons: {
                            showToday: true,
                            showClear: true,
                            showClose: true
                        },
                        date: startDefault,
                        enableHours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 18, 19, 20, 21, 22, 23, 24]
                    });

                    clearInterval(checkExist);
                }
            });

            $('#modal-filters .form-widget-compound > .field-date [type=\'date\']').each((i, elem) => {
                let elId = '#' + elem.id;
                if ($(elId).length) {
                    let startDate = $('#' + elem.id);
                    let startDefault = startDate.val() ? moment(startDate.val()) : moment();
                    startDate.attr({
                        'data-target': elId,
                        'data-toggle': 'datetimepicker',
                        'type': 'text',
                        'readonly': 'readonly'
                    });
                    startDate.addClass('custom-datetimepicker');

                    startDate.datetimepicker({
                        format: 'YYYY-MM-DD',
                        allowInputToggle: true,
                        // locale: 'en',
                        ignoreReadonly: true,
                        buttons: {
                            showToday: true,
                            showClear: true,
                            showClose: true
                        },
                        date: startDefault,
                        enabledHours: false
                    });

                    clearInterval(checkExist);
                }
            });
        }, 100);

    })
});

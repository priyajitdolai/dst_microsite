#!/bin/sh

if [ -z $1 ]
then
    cd /var/app && rm -rf node_modules && npm install && ng build --aot;
else
    cd /var/app && rm -rf node_modules && npm install && ng build --configuration=$1 --aot;
fi

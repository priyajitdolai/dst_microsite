#!/bin/bash

# show commands being executed, per debug
#set -x

now=$(date +"%D %T")
echo "-------------------------Start import at : $now------------------------------------";

# define container name
_docker_php="dst-php" #or _project_folder= "server_path/dst"
# source folder
_source_folder="/var/www/csv_files" #now is path from docker container
# define table name
_table_name="fos_user"

# define directory containing CSV files
_csv_directory="$_source_folder/import/users"
_csv_success_imported="$_source_folder/import_result/users/success"
_csv_errors_imported="$_source_folder/import_result/users/errors"


# go into directory
cd $_csv_directory

# get a list of CSV files in directory
_csv_files=`ls -1 *.csv`

# loop through csv files
for _csv_file in ${_csv_files[@]}
do

    /var/www/bin/console --env=prod  app:import:users /var/www/csv_files/import/users/$_csv_file
    #$_project_folder/symfony/bin/console --env=prod  app:import:users $_project_folder/csv_files/import/users/$_csv_file

    if [ $? -eq 0 ]
        then
            # move file to imported folder
            mv $_csv_directory/$_csv_file $_csv_success_imported/$_csv_file
        else
            mv $_csv_directory/$_csv_file $_csv_errors_imported/$_csv_file
    fi
done
now=$(date +"%D %T")
echo "-------------------------Finish import at : $now------------------------------------";
exit

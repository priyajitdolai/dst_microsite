#!/bin/bash

# show commands being executed, per debug
#set -x

now=$(date +"%D %T")
echo "-------------------------Start import at : $now------------------------------------";

# define database connectivity
_db="dst"
_db_user="dst"
_db_password="root"
_db_host="localhost"

# source folder
_source_folder="/var/www/csv_files" #now is path from docker container
# define table name
_table_name="voucher"

# columns in table keep order to be sure for importing right data in right column
_header_columns_string="voucher_code,supplier,expiry_date,voucher_type,offer_id,offer_name,offer_display,offer_status,sales_channel"


# define directory containing CSV files
_csv_directory="$_source_folder/import/vouchers"
_csv_success_imported="$_source_folder/import_result/vouchers/success"
_csv_errors_imported="$_source_folder/import_result/vouchers/errors"


# go into directory
cd $_csv_directory

# get a list of CSV files in directory
_csv_files=`ls -1 *.csv`

# loop through csv files
for _csv_file in ${_csv_files[@]}
do
    # add 'IGNORE 1 ROWS'  if need to ignore first row
    mysql --host=$_db_host -u$_db_user -p$_db_password $_db --local-infile -e "LOAD DATA LOCAL INFILE '$_csv_directory/$_csv_file' INTO TABLE $_table_name FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n'  ($_header_columns_string)"
    if [ $? -eq 0 ]
        then
            mv $_csv_directory/$_csv_file $_csv_errors_imported/$_csv_file
        else
            # move file to imported folder
            mv $_csv_directory/$_csv_file $_csv_success_imported/$_csv_file
    fi
done
now=$(date +"%D %T")
echo "-------------------------Finish import at : $now------------------------------------";
exit

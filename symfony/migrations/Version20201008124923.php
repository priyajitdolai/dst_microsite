<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201008124923 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS fos_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_957A6479C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS redemption (id BIGINT AUTO_INCREMENT NOT NULL, redeem_date DATETIME DEFAULT NULL, redeem_user VARCHAR(255) DEFAULT NULL, voucher_code VARCHAR(255) NOT NULL, supplier VARCHAR(255) DEFAULT NULL, offer_id VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS user_action (id BIGINT AUTO_INCREMENT NOT NULL, user_name VARCHAR(255) NOT NULL, action VARCHAR(255) NOT NULL, action_status VARCHAR(255) NOT NULL, action_values JSON NOT NULL COMMENT \'(DC2Type:json_array)\', user_ip VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX username_idx (user_name), INDEX action_idx (action), INDEX action_status_idx (action_Status), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS voucher (id BIGINT AUTO_INCREMENT NOT NULL, sales_channel VARCHAR(255) DEFAULT NULL, redeem_date DATETIME DEFAULT NULL, redeem_user VARCHAR(255) DEFAULT NULL, origin VARCHAR(255) DEFAULT NULL, msisdn VARCHAR(255) DEFAULT NULL, expiry_date VARCHAR(255) DEFAULT NULL, voucher_status VARCHAR(255) DEFAULT NULL, voucher_type VARCHAR(255) DEFAULT NULL, voucher_code VARCHAR(255) NOT NULL, offer_name VARCHAR(255) DEFAULT NULL, offer_display VARCHAR(255) DEFAULT NULL, offer_status VARCHAR(255) DEFAULT NULL, offer_id VARCHAR(255) DEFAULT NULL, supplier VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX code_idx (voucher_code), INDEX offer_id_idx (offer_id), INDEX supplier_idx (supplier), INDEX msisdn_idx (msisdn), INDEX redemption_date_idx (redeem_date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('DROP TABLE redemption');
        $this->addSql('DROP TABLE user_action');
        $this->addSql('DROP TABLE voucher');
    }
}

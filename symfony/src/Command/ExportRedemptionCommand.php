<?php

namespace App\Command;

use App\Service\ExportService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Sonata\Exporter\Source\ArraySourceIterator;
use Symfony\Component\Translation\TranslatorInterface;


/**
 * Class ExportRedemptionCommand
 * @package App\Command
 */
class ExportRedemptionCommand extends Command
{
    const FILE_NAME = 'redemption.csv';
    /**
     * @var string
     */
    protected static $defaultName = 'app:export:redemption';

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var string
     */
    private $dailyRedemptionEmails;

    /**
     * @var string
     */
    private $dailyRedemptionFilePath;

    /**
     * @var int
     */
    private $deleteFilesOlderThanNdays;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ExportService
     */
    private $exportService;

    /**
     * @var int
     */
    private $sendRedemptionEmail;


    /**
     * ExportRedemptionCommand constructor.
     * @param EntityManagerInterface $em
     * @param $dailyRedemptionEmails
     * @param $dailyRedemptionFilePath
     * @param $deleteFilesOlderThanNdays
     * @param TranslatorInterface $translator
     * @param ExportService $exportService
     * @param $sendRedemptionEmail
     */
    public function __construct(
        EntityManagerInterface $em,
        $dailyRedemptionEmails,
        $dailyRedemptionFilePath,
        $deleteFilesOlderThanNdays,
        TranslatorInterface $translator,
        ExportService $exportService,
        $sendRedemptionEmail
    ) {
        parent::__construct();

        $this->em = $em;
        $this->dailyRedemptionEmails = $dailyRedemptionEmails;
        $this->dailyRedemptionFilePath = $dailyRedemptionFilePath;
        $this->exportService = $exportService;
        $this->deleteFilesOlderThanNdays = $deleteFilesOlderThanNdays;
        $this->translator = $translator;
        $this->sendRedemptionEmail = $sendRedemptionEmail;
    }

    protected function configure()
    {
        $this
            ->addOption(
                'deleteHistory',
                'dh',
                InputOption::VALUE_OPTIONAL,
                'Delete history than N days ? [value => 1, 0]',
                true
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $deleteHistory = (boolean)$input->getOption('deleteHistory');

            $this->generateFile();

            if (trim($this->dailyRedemptionEmails) && (int)$this->sendRedemptionEmail) {
                $this->sendEmail();
            }

            if ($deleteHistory) {
                $this->cleanHistory();
            }

        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
        }
    }

    /**
     * @throws \Exception
     */
    private function generateFile()
    {
        try {
            $now = new \DateTime();
            $now->modify('-1 days');
            $db = $this->em->getConnection();
            $columns = "voucher_code, supplier, expiry_date, voucher_status, voucher_type,
                        msisdn, offer_id, offer_name, offer_status, sales_channel,
                         'purchaseDate', redeem_date, origin";


            $stm = $db->prepare(
                'SELECT ' . $columns .
                ' FROM voucher WHERE redeem_date<="' . $now->format('Y-m-d 23:59:59') . '" AND redeem_date>="' . $now->format('Y-m-d 00:00:00') . '"'
            );
            $stm->execute();

            $source = new ArraySourceIterator(
                $this->exportService->convertToUTCTime($stm->fetchAll())
            );

            $this->exportService->exportFromArraySourceIterator(
                $source, self::FILE_NAME, $this->dailyRedemptionFilePath
            );
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    private function sendEmail()
    {
        $this->exportService->sendEmail(
            $this->translator->trans('public.export.user_actions.email.subject'),
            self::FILE_NAME,
            $this->dailyRedemptionFilePath,
            $this->dailyRedemptionEmails
        );
    }

    private function cleanHistory()
    {
        $this->cleanFolder();
    }

    private function cleanFolder()
    {
        $this->exportService->cleanFolder($this->dailyRedemptionFilePath);
    }
}

<?php

namespace App\Command;

use App\Service\ExportService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Sonata\Exporter\Source\ArraySourceIterator;
use Symfony\Component\Translation\TranslatorInterface;


/**
 * Class ExportUsersActionsCommand
 */
class ExportUsersActionsCommand extends Command
{
    const FILE_NAME = 'user_actions.csv';
    /**
     * @var string
     */
    protected static $defaultName = 'app:export:users-actions';

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var string
     */
    private $dailyUserActionsEmails;

    /**
     * @var string
     */
    private $dailyUserActionsFilePath;

    /**
     * @var ExportService
     */
    private $exportService;

    /**
     * @var int
     */
    private $deleteFilesOlderThanNdays;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var int
     */
    private $sendUserActionsEmail;

    /**
     * ExportUsersActionsCommand constructor.
     * @param EntityManagerInterface $em
     * @param $dailyUserActionsEmails
     * @param $dailyUserActionsFilePath
     * @param ExportService $exportService
     * @param $deleteFilesOlderThanNdays
     * @param TranslatorInterface $translator
     * @param $sendUserActionsEmail
     */
    public function __construct(
        EntityManagerInterface $em,
        $dailyUserActionsEmails,
        $dailyUserActionsFilePath,
        ExportService $exportService,
        $deleteFilesOlderThanNdays,
        TranslatorInterface $translator,
        $sendUserActionsEmail
    ) {
        parent::__construct();

        $this->em = $em;
        $this->dailyUserActionsEmails = $dailyUserActionsEmails;
        $this->dailyUserActionsFilePath = $dailyUserActionsFilePath;
        $this->exportService = $exportService;
        $this->deleteFilesOlderThanNdays = $deleteFilesOlderThanNdays;
        $this->translator = $translator;
        $this->sendUserActionsEmail = $sendUserActionsEmail;
    }

    protected function configure()
    {
        $this
            ->addOption(
                'deleteHistory',
                'dh',
                InputOption::VALUE_OPTIONAL,
                'Delete history than N days ? [value => 1, 0]',
                true
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $deleteHistory = (boolean)$input->getOption('deleteHistory');

            $this->generateFile();

            if (trim($this->dailyUserActionsEmails) && (int)$this->sendUserActionsEmail) {
                $this->sendEmail();
            }

            if ($deleteHistory) {
                $this->cleanHistory();
            }

        } catch (\Exception $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');
        }
    }

    /**
     * @throws \Exception
     */
    private function generateFile()
    {
        try {
            $now = new \DateTime();
            $db = $this->em->getConnection();
            $stm = $db->prepare('SELECT * FROM user_action WHERE created_at>="' . $now->format("Y-m-d") . ' 00:00:00"');
            $stm->execute();
            $source = new ArraySourceIterator($stm->fetchAll());

            $this->exportService->exportFromArraySourceIterator($source, self::FILE_NAME,
                $this->dailyUserActionsFilePath);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    private function sendEmail()
    {
        $this->exportService->sendEmail(
            $this->translator->trans('public.export.user_actions.email.subject'),
            self::FILE_NAME,
            $this->dailyUserActionsFilePath,
            $this->dailyUserActionsEmails
        );
    }

    private function cleanHistory()
    {
        $this->cleanDB();
        $this->cleanFolder();
    }

    private function cleanFolder()
    {
        $this->exportService->cleanFolder($this->dailyUserActionsFilePath);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    private function cleanDB()
    {
        $now = new \DateTime();
        $now->modify('-' . $this->deleteFilesOlderThanNdays . 'days');
        $db = $this->em->getConnection();
        $stm = $db->prepare('DELETE FROM user_action WHERE created_at<="' . $now->format('Y-m-d') . ' 23:59:59"');
        $stm->execute();
    }
}

<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use FOS\UserBundle\Model\UserManagerInterface;

/**
 * Class ImportUsersCommand
 */
class ImportUsersCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:import:users';

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    public function __construct(EntityManagerInterface $em, UserManagerInterface $userManager)
    {
        parent::__construct();

        $this->em = $em;
        $this->userManager = $userManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->addArgument('filePath', InputArgument::REQUIRED)
            ->addArgument('ignoreFirstRow', InputArgument::OPTIONAL);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $filePath = $input->getArgument('filePath');
            $ignoreFirstRow = $input->getArgument('ignoreFirstRow');

            if (!file_exists($filePath)) {
                $output->writeln('<error>The file ' . $filePath . ' doesn\'t exist</error>');
                return;
            }

            if (($handle = fopen($filePath, "r")) !== false) {
                $i = 0;
                while (($data = fgetcsv($handle, null, ",")) !== false) {
                    $i++;
                    if ($ignoreFirstRow && $i == 1) {
                        continue;
                    }

                    $this->register($data);
                }
                fclose($handle);

                $this->em->flush();
            }
        } catch (\Exception $e) {
            //need to move file in the error folder
            throw new \Exception($e->getMessage());
        }

    }

    /**
     * This method registers an user in the database manually.
     *
     * @param $data
     * @return bool
     */
    private function register($data)
    {
        //TODO: map data from csv file
        $email = $data[0];
        $password = $data[2];

        $emailExist = $this->userManager->findUserByEmail($email);

        if ($emailExist) {
            return true;
        }

        $user = $this->userManager->createUser();

        $user->setUsername($email);
        $user->setEmail($email);
        $user->setEmailCanonical($email);
        $user->setEnabled(1);
        $user->setPlainPassword($password);
        $this->userManager->updateUser($user);

        return true;
    }

}

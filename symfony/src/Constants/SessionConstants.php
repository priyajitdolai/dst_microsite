<?php
namespace App\Constants;

class SessionConstants{
    const BLOCKED_UNTIL = 'blockedLoginUntil';
    const BAD_ATTEMPTS = 'badAttempts';

}
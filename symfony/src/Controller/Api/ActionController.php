<?php

namespace App\Controller\Api;

use App\Service\UserActionsService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VoucherController
 * @Route("api/action/")
 */
class ActionController extends AbstractController
{

    /**
     * @Route("log", name="api_log_action", methods={"POST"})
     * @param Request $request
     * @param \App\Service\UserActionsService $userActionsService
     * @param \Psr\Log\LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function logAction(
        Request $request,
        UserActionsService $userActionsService,
        LoggerInterface $logger
    ) : JsonResponse
    {
        $params = [];
        if ($content = $request->getContent()) {
            $params = json_decode($content, true);
        }

        if (
            !array_key_exists('username', $params) ||
            !array_key_exists('action', $params) ||
            !array_key_exists('status', $params) ||
            !array_key_exists('values', $params)
        ) {
            return new JsonResponse(
                ['message' => 'You should provide username, action, status, values to log action'],
                Response::HTTP_BAD_REQUEST
            );
        }

        try {
            $userActionsService->collectData($params, $request);
        } catch (\Exception $e) {
            $logger->error($e->getMessage());

            return new JsonResponse(['message' => 'Can\'t save action log in DB']);
        }


        return new JsonResponse(['message' => 'success']);
    }

}

<?php

namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class SecurityController
 * @package App\Controller\Api
 * @Route("api/")
 */
class SecurityController
{
    /**
     * @Route("me", name="api_get_me", methods={"GET"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @param Security $security
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function getMe(Security $security, SerializerInterface $serializer)
    {
        return new JsonResponse($serializer->normalize($security->getUser()));
    }

    // FIXME keep for next
}

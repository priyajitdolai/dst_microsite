<?php

namespace App\Controller\Api;

use App\Entity\UserAction;
use App\Entity\Voucher;
use App\Service\UserActionsService;
use App\Service\VoucherService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class VoucherController
 *
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 *
 * @Route("api/voucher/")
 */
class VoucherController extends AbstractController
{
    /**
     * @Route("search", name="api_voucher_search", methods={"GET"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param SerializerInterface $serializer
     * @param UserActionsService $userActionsService
     * @param LoggerInterface $logger
     * @return JsonResponse
     */
    public function searchVoucher(
        Request $request,
        EntityManagerInterface $em,
        SerializerInterface $serializer,
        UserActionsService $userActionsService,
        LoggerInterface $logger
    )
    {
        $params = $request->query->all();

        if (!isset($params['voucherCode'])) {
            return new JsonResponse(['message' => 'Voucher code was not given'], Response::HTTP_BAD_REQUEST);
        }

        $code = $params['voucherCode'];
        $msisdn = $params['msisdn'] ?? null;

        $data = [
            'voucherCode' => $code,
        ];

        $voucher = $em->getRepository(Voucher::class)->findOneBy($data);

        if ($msisdn) {
            $data['msisdn'] = $msisdn;
        }
        $this->collectData(
            $userActionsService,
            $request,
            UserAction::ACTION_CHECK_CODE,
            $data,
            $voucher ? UserAction::STATUS_SUCCESS : UserAction::STATUS_ERROR,
            $logger
        );

        if (!$voucher) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($serializer->normalize($voucher));
    }

    /**
     * @Route("redeem/{voucherCode}", name="api_voucher_redeem", methods={"POST"})
     *
     * @param Request $request
     * @param $voucherCode
     * @param VoucherService $voucherService
     * @param EntityManagerInterface $em
     * @param SerializerInterface $serializer
     * @param UserActionsService $userActionsService
     * @param LoggerInterface $logger
     * @return JsonResponse
     */
    public function redeemVoucher(
        Request $request,
        $voucherCode,
        VoucherService $voucherService,
        EntityManagerInterface $em,
        SerializerInterface $serializer,
        UserActionsService $userActionsService,
        LoggerInterface $logger
    )
    {
        /** @var Voucher $voucher */
        $voucher = $em->getRepository(Voucher::class)->findOneBy(['voucherCode' => $voucherCode]);

        if (!$voucher) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $data = [
            'voucherCode' => $voucher->getVoucherCode()
        ];

        $formData = json_decode($request->getContent(), true);
        if(isset($formData['redeem']) && $formData['redeem']){
            $data['msisdn'] = $formData['redeem']['msisdn'];
        }
        
        $status = 'error';
        $statusCode = 400;
        $response = ['message' => 'not_found'];
        try {
            $voucher = $voucherService->redeemVoucher($data);
            if ($voucher) {
                $em->refresh($voucher);
                $status = 'success';
                $statusCode = 200;
                $response = $serializer->normalize($voucher);
            }
        } catch (\Exception $e) {
            $response = ['message' => $e->getMessage()];
        }

        $this->collectData(
            $userActionsService,
            $request,
            UserAction::ACTION_REDEEM_CODE,
            $data,
            $status == 'success' ? UserAction::STATUS_SUCCESS : UserAction::STATUS_ERROR,
            $logger
        );

        return new JsonResponse($response, $statusCode);
    }

    private function collectData($userActionsService, $request, $action, $data, $status, $logger)
    {
        try {
            $userActionsService->collectData([
                'username' => $this->getUser()->getUsername(),
                'action' => $action,
                'status' => $status,
                'values' => $data,
            ],
                $request
            );
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
        }
    }
}

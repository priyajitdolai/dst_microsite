<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PublicController
 *
 * @Route("/")
 */
class PublicController extends AbstractController
{
    /**
     * @Route("/blocked-login", name="blocked_login", methods={"GET"})
     *
     * @param $timeForBlockedPage
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function blockedLoginAction(Request $request, $timeForBlockedPage)
    {
        $session = $request->getSession();
        $blockedTime = $session->get('blockedLoginUntil');

        $now = new \DateTime();

        if ($blockedTime <= $now) {
            $session->remove('blockedLoginUntil');
            return $this->redirectToRoute('fos_user_security_login');
        }

        $diff = $blockedTime->diff($now);

        return $this->render('pages/blocked-login.html.twig', [
            'nMins' => $diff->i,
            'nSec' => $diff->s
        ]);
    }

}
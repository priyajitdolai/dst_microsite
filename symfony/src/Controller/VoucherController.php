<?php

namespace App\Controller;

use App\Entity\UserAction;
use App\Entity\Voucher;
use App\Form\Type\CodeCheckFormType;
use App\Service\UserActionsService;
use App\Service\VoucherService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VoucherController
 *
 * @IsGranted("ROLE_USER")
 *
 * @Route("/")
 */
class VoucherController extends AbstractController
{
    /**
     * @Route("/", name="voucher_check", methods={"GET","POST"})
     *
     * @param Request $request
     * @param VoucherService $voucherService
     * @param UserActionsService $userActionsService
     * @param LoggerInterface $logger
     * @param $useVoucherCheckCaptcha
     * @return Response
     */
    public function index(
        Request $request,
        VoucherService $voucherService,
        UserActionsService $userActionsService,
        LoggerInterface $logger,
        $useVoucherCheckCaptcha
    ) {
        $voucher = null;
        $inputCode = null;
        $inputMsisdn = null;
        $result = [
            'voucher' => null,
            'errorMessage' => ''
        ];

        if ($request->isMethod(Request::METHOD_POST)) {
            $data = $request->request->get('code_check_form');
            unset($data['_token']);
            $inputCode = $data['voucherCode'];
            $inputMsisdn = $data['msisdn'];

            $result = $voucherService->getVoucher($data);

            $this->collectData(
                $userActionsService,
                $request,
                UserAction::ACTION_CHECK_CODE,
                $data,
                $result['voucher'] ? UserAction::STATUS_SUCCESS : UserAction::STATUS_ERROR,
                $logger
            );
        }

        $form = $this->getForm();

        return $this->render('pages/check-code.html.twig', [
            'form' => $form->createView(),
            'useVoucherCheckCaptcha' => $useVoucherCheckCaptcha,
            'voucher' => $result['voucher'],
            'errorMessage' => $result['errorMessage'],
            'redeem' => null,
            'inputData' => [
                'voucherCode' => $inputCode,
                'msisdn' => $inputMsisdn
            ]
        ]);
    }

    /**
     * @Route("/redeem", name="voucher_redeem", methods={"POST"})
     *
     * @param Request $request
     * @param VoucherService $voucherService
     * @param UserActionsService $userActionsService
     * @param LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redeemVoucher(
        Request $request,
        VoucherService $voucherService,
        UserActionsService $userActionsService,
        LoggerInterface $logger
    ) {
        $data = $request->request->get('redeem');
        $code = $data['voucherCode'];
        try {
            $status = 'success';

            $voucher = $voucherService->redeemVoucher($data);

            if (!$voucher) {
                $status = 'error';
            }

        } catch (\Exception $e) {
            $status = 'error';
        }

        $this->collectData(
            $userActionsService,
            $request,
            UserAction::ACTION_REDEEM_CODE,
            $data,
            $status == 'success' ? UserAction::STATUS_SUCCESS : UserAction::STATUS_ERROR,
            $logger
        );

        return $this->redirectToRoute('voucher_redeem_result', [
            'code' => $code,
            'status' => $status,
        ]);
    }

    /**
     * @Route("/redeem-result/{code}/{status}", name="voucher_redeem_result", methods={"GET"})
     *
     * @param $code
     * @param $status
     * @param $useVoucherCheckCaptcha
     * @return Response
     */
    public function resultRedeemVoucher($code, $status, $useVoucherCheckCaptcha)
    {
        $form = $this->getForm();
        $now = new \DateTime();

        return $this->render('pages/check-code.html.twig', [
            'form' => $form->createView(),
            'useVoucherCheckCaptcha' => $useVoucherCheckCaptcha,
            'voucher' => null,
            'errorMessage' => '',
            'redeem' => [
                'status' => $status,
                'voucherCode' => $code,
                'time' => $now->format('d-m-Y H:i:s')
            ],
            'inputData' => [
                'voucherCode' => '',
                'msisdn' => ''
            ]
        ]);
    }

    private function getForm()
    {
        return $this->createForm(CodeCheckFormType::class, new Voucher());
    }

    private function collectData($userActionsService, $request, $action, $data, $status, $logger)
    {
        try {
            $userActionsService->collectData([
                'username' => $this->getUser()->getUsername(),
                'action' => $action,
                'status' => $status,
                'values' => $data,
            ],
                $request
            );
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
        }
    }

}
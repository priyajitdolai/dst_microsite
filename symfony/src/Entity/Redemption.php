<?php

namespace App\Entity;

use App\Entity\Traits\Id;
use App\Entity\Traits\DateTimeTrait;
use App\Entity\Traits\Offer\OfferID;
use App\Entity\Traits\SupplierTrait;
use App\Entity\Traits\Voucher\VoucherCode;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="redemption")
 * @ORM\HasLifecycleCallbacks()
 */
class Redemption
{
    use Id;
    use VoucherCode;
    use SupplierTrait;
    use OfferID;
    use DateTimeTrait;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $redeemDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $redeemUser;

    /**
     * @return mixed
     */
    public function getRedeemDate()
    {
        return $this->redeemDate;
    }

    /**
     * @param mixed $redeemDate
     * @return Redemption
     */
    public function setRedeemDate($redeemDate)
    {
        $this->redeemDate = $redeemDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRedeemUser()
    {
        return $this->redeemUser;
    }

    /**
     * @param mixed $redeemUser
     * @return Redemption
     */
    public function setRedeemUser($redeemUser)
    {
        $this->redeemUser = $redeemUser;

        return $this;
    }

}
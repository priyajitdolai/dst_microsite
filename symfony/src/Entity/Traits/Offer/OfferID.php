<?php

namespace App\Entity\Traits\Offer;

use Doctrine\ORM\Mapping as ORM;

Trait OfferID
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $offerID;

    /**
     * @return mixed
     */
    public function getOfferID()
    {
        return $this->offerID;
    }

    /**
     * @param mixed $offerID
     * @return mixed
     */
    public function setOfferID($offerID = null)
    {
        $this->offerID = $offerID;

        return $this;
    }

}
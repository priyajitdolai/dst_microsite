<?php

namespace App\Entity\Traits;

use App\Entity\Traits\Offer\OfferID;
use Doctrine\ORM\Mapping as ORM;

Trait OfferTrait
{
    use OfferID;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $offerName;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $offerDisplay;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $offerStatus;

    /**
     * @return mixed
     */
    public function getOfferName()
    {
        return $this->offerName;
    }

    /**
     * @param mixed $offerName
     * @return OfferTrait
     */
    public function setOfferName($offerName)
    {
        $this->offerName = $offerName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOfferDisplay()
    {
        return $this->offerDisplay;
    }

    /**
     * @param mixed $offerDisplay
     * @return mixed
     */
    public function setOfferDisplay($offerDisplay = null)
    {
        $this->offerDisplay = $offerDisplay;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOfferStatus()
    {
        return $this->offerStatus;
    }

    /**
     * @param mixed $offerStatus
     * @return mixed
     */
    public function setOfferStatus($offerStatus = null)
    {
        $this->offerStatus = $offerStatus;

        return $this;
    }

}
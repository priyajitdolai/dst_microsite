<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

Trait SupplierTrait
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $supplier;

    /**
     * @return mixed
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param mixed $supplier
     * @return mixed
     */
    public function setSupplier($supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }


}
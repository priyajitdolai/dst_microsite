<?php

namespace App\Entity\Traits\Voucher;

use Doctrine\ORM\Mapping as ORM;

Trait VoucherCode
{
    /**
     * @ORM\Column(type="string")
     */
    protected $voucherCode;

    /**
     * @return mixed
     */
    public function getVoucherCode()
    {
        return $this->voucherCode;
    }

    /**
     * @param mixed $voucherCode
     * @return mixed
     */
    public function setVoucherCode($voucherCode)
    {
        $this->voucherCode = $voucherCode;

        return $this;
    }

}
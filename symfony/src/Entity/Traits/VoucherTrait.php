<?php

namespace App\Entity\Traits;

use App\Entity\Traits\Voucher\VoucherCode;
use Doctrine\ORM\Mapping as ORM;

Trait VoucherTrait
{
    use VoucherCode;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="date", nullable=true)
     */
    protected $expiryDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $voucherStatus;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $voucherType;

    /**
     * @return mixed
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param mixed $expiryDate
     * @return mixed
     */
    public function setExpiryDate($expiryDate = null)
    {
        $this->expiryDate = $expiryDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVoucherStatus()
    {
        return $this->voucherStatus;
    }

    /**
     * @param mixed $voucherStatus
     * @return mixed
     */
    public function setVoucherStatus($voucherStatus = null)
    {
        $this->voucherStatus = $voucherStatus;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVoucherType()
    {
        return $this->voucherType;
    }

    /**
     * @param mixed $voucherType
     * @return mixed
     */
    public function setVoucherType($voucherType = null)
    {
        $this->voucherType = $voucherType;

        return $this;
    }

}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Id;
use App\Entity\Traits\DateTimeTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_action", indexes={
 *     @ORM\Index(name="username_idx", columns={"user_name"}),
 *     @ORM\Index(name="action_idx", columns={"action"}),
 *     @ORM\Index(name="action_status_idx", columns={"action_Status"})
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class UserAction
{
    const ACTION_LOGIN = "login";
    const ACTION_CHECK_CODE = "check_code";
    const ACTION_REDEEM_CODE = "redeem_code";


    const STATUS_SUCCESS = "success";
    const STATUS_ERROR = "error";

    use Id;
    use DateTimeTrait;
    /**
     * @ORM\Column(type="string")
     */
    protected $userName;

    /**
     * @ORM\Column(type="string")
     */
    protected $action;

    /**
     * @ORM\Column(type="string")
     */
    protected $actionStatus;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $actionValues;

    /**
     * @ORM\Column(type="string")
     */
    protected $userIp;

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     * @return UserAction
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return UserAction
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActionStatus()
    {
        return $this->actionStatus;
    }

    /**
     * @param mixed $actionStatus
     * @return UserAction
     */
    public function setActionStatus($actionStatus)
    {
        $this->actionStatus = $actionStatus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActionValues()
    {
        return $this->actionValues;
    }

    /**
     * @param mixed $actionValues
     * @return UserAction
     */
    public function setActionValues($actionValues)
    {
        $this->actionValues = $actionValues;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * @param mixed $userIp
     * @return UserAction
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;
        return $this;
    }

}
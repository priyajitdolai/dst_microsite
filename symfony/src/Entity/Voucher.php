<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Id;
use App\Entity\Traits\VoucherTrait;
use App\Entity\Traits\OfferTrait;
use App\Entity\Traits\SupplierTrait;
use App\Entity\Traits\DateTimeTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="voucher", indexes={
 *     @ORM\Index(name="code_idx", columns={"voucher_code"}),
 *     @ORM\Index(name="offer_id_idx", columns={"offer_id"}),
 *     @ORM\Index(name="supplier_idx", columns={"supplier"}),
 *     @ORM\Index(name="msisdn_idx", columns={"msisdn"}),
 *     @ORM\Index(name="redemption_date_idx", columns={"redeem_date"})
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class Voucher
{
    use Id;
    use VoucherTrait;
    use OfferTrait;
    use SupplierTrait;
    use DateTimeTrait;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $salesChannel;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $redeemDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $redeemUser;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $origin;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $msisdn;

    public function __construct()
    {
        $this->expiryDate = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getSalesChannel()
    {
        return $this->salesChannel;
    }

    /**
     * @param mixed $salesChannel
     * @return Voucher
     */
    public function setSalesChannel($salesChannel = null)
    {
        $this->salesChannel = $salesChannel;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getRedeemDate()
    {
        return $this->redeemDate;
    }

    /**
     * @param \DateTime|null $redeemDate
     * @return Voucher
     */
    public function setRedeemDate(?\DateTime $redeemDate = null)
    {
        $this->redeemDate = $redeemDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRedeemUser()
    {
        return $this->redeemUser;
    }

    /**
     * @param mixed $redeemUser
     * @return Voucher
     */
    public function setRedeemUser($redeemUser = null)
    {
        $this->redeemUser = $redeemUser;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param mixed $origin
     * @return Voucher
     */
    public function setOrigin($origin = null)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMsisdn(): ?string
    {
        return $this->msisdn;
    }

    /**
     * @param string|null $msisdn
     *
     * @return $this
     */
    public function setMsisdn(?string $msisdn): self
    {
        $this->msisdn = $msisdn;

        return $this;
    }

}

<?php

namespace App\EventHandler;

use App\Constants\SessionConstants;
use App\Entity\User;
use App\Entity\UserAction;
use App\Service\UserActionsService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;


class AuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{
    /**
     * @var UserActionsService
     */
    private $userActionsService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    private $badAttempts;

    private $timeForBlockedPage;

    public function __construct(
        UserActionsService $userActionsService,
        LoggerInterface $logger,
        UrlGeneratorInterface $router,
        $badAttempts,
        $timeForBlockedPage
    ) {
        $this->userActionsService = $userActionsService;
        $this->logger = $logger;
        $this->router = $router;
        $this->badAttempts = $badAttempts;
        $this->timeForBlockedPage = $timeForBlockedPage;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->collectData($request, UserAction::STATUS_SUCCESS);

        $userRoles = $token->getUser()->getRoles();

        if (in_array(User::ROLE_ADMIN, $userRoles)) {
            return new RedirectResponse($this->router->generate('easyadmin'));
        }

        return new RedirectResponse($this->router->generate('voucher_check'));
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->collectData($request, UserAction::STATUS_ERROR);
        $session = $request->getSession();
        $session->getFlashBag()->set('error', $exception->getMessage());

        $badAttempts = $session->get(SessionConstants::BAD_ATTEMPTS);
        $badAttempts++;
        $session->set(SessionConstants::BAD_ATTEMPTS, $badAttempts);

        if ($badAttempts >= $this->badAttempts) {
            $session->remove(SessionConstants::BAD_ATTEMPTS);
            $now = new \DateTime();
            $now->modify('+' . $this->timeForBlockedPage . 'minutes');
            $session->set(SessionConstants::BLOCKED_UNTIL, $now);

            return new RedirectResponse($this->router->generate('blocked_login'));
        }

        return new RedirectResponse($this->router->generate('fos_user_security_login'));
    }

    /**
     * @param $request
     * @param $status
     */
    private function collectData($request, $status)
    {
        $username = $request->request->get('_username');
        $password = $request->request->get('_password');

        try {
            $this->userActionsService->collectData([
                'username' => $username,
                'action' => UserAction::ACTION_LOGIN,
                'status' => $status,
                'values' => [
                    'username' => $username,
                    'password' => $password
                ],
            ],
                $request
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
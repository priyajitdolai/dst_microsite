<?php

namespace App\Gateway;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * class CaptchaChecker
 */
class CaptchaChecker
{
    /** @var string */
    private $secret;

    /** @var bool */
    private $sandbox;

    /**
     * @param string $secret
     * @param bool   $sandbox
     */
    public function __construct(string $secret, bool $sandbox = false)
    {
        $this->secret = $secret;
        $this->sandbox = $sandbox;
    }

    /**
     * @param string $captchaValue
     *
     * @return bool
     * @throws TransportExceptionInterface
     */
    public function check(string $captchaValue): bool
    {
        if ($this->sandbox) {
            return true;
        }

        $response = $this->getCaptchaResponse($captchaValue);

        if ($response && isset($response['success']) && true === $response['success']) {
            return true;
        }

        return false;
    }

    /**
     * @param string $captchaValue
     *
     * @return array
     * @throws TransportExceptionInterface
     */
    private function getCaptchaResponse(string $captchaValue): array
    {
        $response = $this->getClient()->request(
            'POST',
            'https://www.google.com/recaptcha/api/siteverify',
            [
                'json' => [
                    'secret'   => $this->secret,
                    'response' => $captchaValue,
                ],
            ]
        );

        return json_decode($response->getBody(), true);
    }

    /**
     * @return HttpClientInterface
     */
    private function getClient(): HttpClientInterface
    {
        return HttpClient::create();
    }
}

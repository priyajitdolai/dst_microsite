<?php

namespace App\Serializer\Normalizer;

use App\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class BaseTalentNormalizer
 */
class UserNormalizer implements ContextAwareNormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    /** @var  TranslatorInterface */
    private $translator;

    /** @var Security */
    private $security;

    /** @var EntityManagerInterface */
    private $em;

    /**
     * @param TranslatorInterface $translator
     * @param Security $security
     * @param EntityManagerInterface $em
     */
    public function __construct(TranslatorInterface $translator, Security $security, EntityManagerInterface $em)
    {
        $this->translator = $translator;
        $this->security = $security;
        $this->em = $em;
    }

    /**
     * @var User $object
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'username' => $object->getUsername(),
            'email' => $object->getEmail(),
            'enabled' => $object->isEnabled(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof User || $data instanceof UserInterface;
    }
}

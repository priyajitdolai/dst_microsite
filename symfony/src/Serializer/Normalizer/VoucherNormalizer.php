<?php

namespace App\Serializer\Normalizer;

use App\Entity\Voucher;
use DateTimeInterface;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class BaseTalentNormalizer
 */
class VoucherNormalizer implements ContextAwareNormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    /** @var  TranslatorInterface */
    private $translator;

    /** @var Security */
    private $security;

    /** @var EntityManagerInterface */
    private $em;

    /**
     * @param TranslatorInterface $translator
     * @param Security $security
     * @param EntityManagerInterface $em
     */
    public function __construct(TranslatorInterface $translator, Security $security, EntityManagerInterface $em)
    {
        $this->translator = $translator;
        $this->security = $security;
        $this->em = $em;
    }

    /**
     * @var Voucher $object
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'voucherCode' => $object->getVoucherCode(),
            'salesChannel' => $object->getSalesChannel(),
            'redeemDate' => $object->getRedeemDate() ? $object->getRedeemDate()->format(DateTimeInterface::W3C) : null,
            'redeemUser' => $object->getRedeemUser(),
            'msisdn' => $object->getMsisdn(),
            'expiryDate' => $object->getExpiryDate(),
            'voucherType' => $object->getVoucherType(),
            'offerName' => $object->getOfferName(),
            'offerDisplay' => $object->getOfferDisplay(),
            'offerStatus' => $object->getOfferStatus(),
            'offerId' => $object->getOfferID(),
            'supplier' => $object->getSupplier(),
            'createdAt' => $object->getCreatedAt()->format(DateTimeInterface::W3C),
            'updatedAt' => $object->getUpdatedAt() ? $object->getUpdatedAt()->format(DateTimeInterface::W3C) : null,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof Voucher;
    }
}

<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\Exporter\Handler;
use Sonata\Exporter\Source\ArraySourceIterator;
use Sonata\Exporter\Writer\CsvWriter;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Validator\Constraints\Date;

class ExportService
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * @var string
     */
    private $defaultFrom;

    /**
     * @var MailerInterface $mailer
     */
    private $mailer;

    /**
     * @var int
     */
    private $deleteFilesOlderThanNdays;

    /**
     * @param EntityManagerInterface $em
     * @param $defaultFrom
     * @param MailerInterface $mailer
     * @param $deleteFilesOlderThanNdays ,
     */
    public function __construct(
        EntityManagerInterface $em,
        $defaultFrom,
        MailerInterface $mailer,
        $deleteFilesOlderThanNdays
    ) {
        $this->em = $em;
        $this->defaultFrom = $defaultFrom;
        $this->mailer = $mailer;
        $this->deleteFilesOlderThanNdays = $deleteFilesOlderThanNdays;
    }

    /**
     * @param ArraySourceIterator $source
     * @param string $fileName
     * @param string $filePath
     */
    function exportFromArraySourceIterator(ArraySourceIterator $source, string $fileName, string $filePath)
    {
        $writer = new CsvWriter($filePath . '/' . $this->prepareFileName($fileName));
        Handler::create($source, $writer)->export();
    }

    function convertToUTCTime($data)
    {
        foreach ($data as $key => $line) {
            array_walk($line, function (&$value, $key) {
                $columnsToConvert = ['expiry_date', 'redeem_date'];
                if (in_array($key, $columnsToConvert)) {
                    $value = (new \DateTime($value))->format('c');
                }
                if ($key == 'purchaseDate') {
                    $value = '';
                }

            });
            $newLine = [];
            foreach ($line as $column => $value) {
                $newLine[$this->adaptCsvHeaderNames($column)] = $value;
            }

            $data[$key] = $newLine;
        }

        return $data;
    }

    public function adaptCsvHeaderNames($key)
    {
        $key = $this->camelCase($key);
        if ($key == 'offerId') {
            $key = 'offerID';
        }

        return $key;
    }

    function camelCase($input, $separator = '_')
    {
        return lcfirst(str_replace($separator, '', ucwords($input, $separator)));
    }

    /**
     * @param string $fileName
     * @return string
     */
    private function prepareFileName(string $fileName)
    {
        $now = new \DateTime();
        $now->modify('-1 days');

        return $now->format('d_m_Y') . '_' . $fileName;
    }

    /**
     * @param string $subject
     * @param string $fileName
     * @param string $filePath
     * @param string $destinationEmails
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Exception
     */
    function sendEmail(string $subject, string $fileName, string $filePath, string $destinationEmails)
    {
        $email = (new TemplatedEmail())
            ->from($this->defaultFrom);

        $email = $this->emailTo($email, $destinationEmails);

        $email->subject($subject)
            ->htmlTemplate('emails/daily.html.twig')
            ->attachFromPath($filePath . '/' . $this->prepareFileName($fileName));

        $this->mailer->send($email);
    }

    /**
     * @param $email
     * @param string $destinationEmails
     * @return mixed
     * @throws \Exception
     */
    private function emailTo($email, string $destinationEmails)
    {
        $emails = explode(',', $destinationEmails);
        $nbEmails = count($emails);

        if ($nbEmails && trim($emails[0])) {
            $email->to($emails[0]);
            if ($nbEmails > 1) {
                for ($i = 1; $i < $nbEmails; $i++) {
                    if ($emails[$i]) {
                        $email->addTo($emails[$i]);
                    }
                }
            }
        } else {
            throw new \Exception('Define values for parameter: DAILY_USER_ACTIONS_EMAILS');
        }

        return $email;
    }

    /**
     * @param string $filePath
     */
    function cleanFolder(string $filePath)
    {
        $fileTypesToDelete = array("csv");
        if ($handle = opendir($filePath)) {
            while (false !== ($file = readdir($handle))) {
                $filePath = $filePath . '/' . $file;
                if (is_file($filePath)) {
                    $fileInfo = pathinfo($filePath);
                    if (isset($fileInfo['extension']) && in_array(strtolower($fileInfo['extension']),
                            $fileTypesToDelete)) {
                        if (filemtime($filePath) < (time() - ($this->deleteFilesOlderThanNdays * 24 * 60 * 60))) {
                            unlink($filePath);
                        }
                    }
                }
            }
        }
    }
}

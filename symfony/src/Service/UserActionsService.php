<?php

namespace App\Service;

use App\Entity\UserAction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;


class UserActionsService
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * @param $data
     * @param Request $request
     */
    public function collectData(array $data, Request $request)
    {
        $actions = (new UserAction())
            ->setUserIp($request->getClientIp())
            ->setUserName($data['username'])
            ->setAction($data['action'])
            ->setActionStatus($data['status'])
            ->setActionValues($data['values']);

        $this->em->persist($actions);
        $this->em->flush();
    }
}

<?php

namespace App\Service;

use App\Entity\Redemption;
use App\Entity\Voucher;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;


class VoucherService
{
    /** @var Security */
    private $security;

    /** @var EntityManagerInterface */
    private $em;

    /**
     * @var  LoggerInterface
     */
    private $logger;

    /**
     * VoucherService constructor.
     * @param Security $security
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     */
    public function __construct(
        Security $security,
        EntityManagerInterface $em,
        LoggerInterface $logger
    ) {
        $this->security = $security;
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @param $data
     * @return array
     */
    public function getVoucher($data)
    {
        $voucher = null;
        $error = '';
        if ($data && $data['voucherCode']) {
            $voucher = $this->findVoucher($data['voucherCode'], false);
        }
        if ($voucher) {
            if ($voucher->getRedeemDate()) {
                $error = 'code_already_redeemed';
                $voucher = null;
            }
        } else {
            $error = 'code_unavailable';
        }

        return [
            'voucher' => $voucher,
            'errorMessage' => $error
        ];
    }

    /**
     * @param $data
     * @return null|object
     */
    public function redeemVoucher($data)
    {
        $voucher = null;
        if ($data) {
            $code = $data['voucherCode'];
            $msisdn = $data['msisdn'];

            $voucher = $this->findVoucher($code);

            if ($voucher) {
                $authUser = $this->security->getUser();
                $voucher
                    ->setMsisdn($msisdn)
                    ->setRedeemUser($authUser->getUsername())
                    ->setRedeemDate(new \DateTime());

                $this->em->persist($voucher);
                $this->em->flush();

                $this->saveHistory($voucher);
            }
        }

        return $voucher;
    }

    /**
     * @param $code
     * @param bool $withRedeem
     * @return null|object
     */
    private function findVoucher($code, $withRedeem = true)
    {
        $data = [
            'voucherCode' => $code
        ];
        if ($withRedeem) {
            $data['redeemDate'] = null;
        }

        return $this->em->getRepository(Voucher::class)->findOneBy($data);
    }

    /**
     * @param Voucher $voucher
     */
    private function saveHistory(Voucher $voucher)
    {
        try {
            $redeem = (new Redemption())
                ->setRedeemDate($voucher->getRedeemDate())
                ->setRedeemUser($voucher->getRedeemUser())
                ->setVoucherCode($voucher->getVoucherCode())
                ->setOfferID($voucher->getOfferID())
                ->setSupplier($voucher->getSupplier());

            $this->em->persist($redeem);
            $this->em->flush();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
